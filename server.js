const express = require('express');
const bodyParser = require('body-parser');
const { Client } = require('pg');
const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand')
const app = express();
const port = 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var ctEnv = dotenv.config({path:__dirname+'/./db.env'})
dotenvExpand(ctEnv)


app.post('/oenace_data', async (req, res) => {
	console.log("oenace_data request");
	const client = new Client({ connectionString: process.env.DATABASE_URL });
	let q_text;
	try {
		await client.connect();
		console.log('connected');
		q_text = `select edv_code as edv_e1, initcap(kurztitel) as desc from oenace2008en where ebene = 1 order by edv_e1;`;
	} catch (err) {
		console.log(err.message, err.stack)
		res.send(null);
	}
	try {
		ret = await client.query(q_text);

		if (ret.rows.length == 0) {
			throw new Error('query empty');
		} else {
			console.log("oenace contains " + ret.rows.length + " items");
			res.send(ret.rows);
		}
	} catch (err) {
		console.log(err.message, err.stack)
		res.send(null);
	}finally{
		client.end()
		.then(() => console.log("disconnected"))
		.catch (err => console.log('disconnection error', err.stack));
	}
});

app.post('/available_models', async (req, res) => {
	console.log("Retrieving models");
	const client = new Client({ connectionString: process.env.DATABASE_URL });
	console.log(process.env.DATABASE_URL);
	try {
		await client.connect();
		console.log('connected');
		let q_text = `SELECT * FROM model_labels;`;
		try {
			localData = await client.query(q_text);
			if (localData.rows.length == 0) {
				throw new Error('query empty');
			} else {
				console.log("Result contains " + localData.rows.length + " items");
				res.send(localData.rows);
			}

		} catch (err) {
			console.log(err.message, err.stack)
			res.send(null);
		}
	} catch (err) {
		console.log("connection error", err.stack)
		res.send(null);
	}finally{
		client.end()
		.then(() => console.log("disconnected"))
		.catch (err => console.log('disconnection error', err.stack));	
	}
});

app.post('/local_data', async (req, res) => {
	console.log("local_data request: " + JSON.stringify(req.body));
	const client = new Client({ connectionString: process.env.DATABASE_URL });
	console.log(process.env.DATABASE_URL);
	try {
		await client.connect();
		console.log('connected');
		let q_text = ``;
		try {
			let localData;
			if(!req.body.model_options.filtered){
				q_text = `SELECT location.firm_id, latitude, longitude, edv_e1
					FROM (location join sector on location.firm_id = sector.firm_id)
					WHERE location.is_hq = 't' AND sector.is_main ='t' ` 
					+ `ORDER BY location.firm_id `;		
				localData = await client.query(q_text);						
			}else{
				q_text = `SELECT location.firm_id, latitude, longitude, edv_e1
					FROM (location join sector on location.firm_id = sector.firm_id)
					WHERE location.is_hq = 't' AND sector.is_main ='t' ` 
					+ ` AND location.firm_id IN ((SELECT source FROM model_edge WHERE id = $1 AND weight > 0) UNION (SELECT target FROM model_edge WHERE id = $1 AND weight > 0))` //#EXTRA LINE FOR FILTERING
					+ `ORDER BY location.firm_id `;
				localData = await client.query(q_text, [req.body.model]);		
			}


			if (localData.rows.length == 0) {
				throw new Error('query empty');
			} else {
				console.log("in " + req.body.view + " result contains " + localData.rows.length + " items");
				res.send(localData.rows);
			}

			client.end()
			.then(() => console.log("disconnected"))
			.catch (err => console.log('disconnection error', err.stack));
		} catch (err) {
			console.log(err.message, err.stack)
			res.send(null);
		}
	} catch (err) {
		console.log("connection error", err.stack)
		res.send(null);
	}
});

app.post('/eu_ratios_data', async (req, res) => {
	console.log("eu_ratios_data request");

	const client = new Client({ connectionString: process.env.DATABASE_URL });
	let q_ratios, q_intervals;
	try {
		await client.connect();
		console.log('connected');
		q_ratios = `select firm_id, bilanzindex, cash_flow from eu_ratios order by bilanzindex`;
		q_intervals = `select count(bilanzindex) from eu_ratios group by bilanzindex order by bilanzindex;`
	} catch (err) {
		console.log(err.message, err.stack)
		res.send(null);
	}
	try {
		let ratios = await client.query(q_ratios);
		let intervals = await client.query(q_intervals);

		if (ratios.rows.length == 0 || intervals.rows.length == 0) {
			throw new Error('query empty');
		} else {
			console.log("eu_ratios contains " + ratios.rows.length + " items");
			console.log("eu_ratios_intervals contains " + intervals.rows.length + " items");
			let accu = [];
			accu.push(0);
			intervals.rows.reduce(
					(ac, curr) => {
						let last = accu[accu.length - 1];
						accu.push(last + Number(curr.count));
						return ac + curr;
					},
					0
			);

			res.send({
				ratios: ratios.rows,
				intervals: accu,
			});
		}
	} catch (err) {
		console.log(err.message, err.stack)
		res.send(null);
	}

	client.end()
	.then(() => console.log("disconnected"))
	.catch (err => console.log('disconnection error', err.stack));
});


app.post('/regional_data', async (req, res) => {
	console.log("regional_data request: " + JSON.stringify(req.body));
	const client = new Client({ connectionString: process.env.DATABASE_URL });
	try {
		await client.connect();
		console.log('connected');
		let q_text;
		if (req.body.regionLevel === 'bundesland') {
			q_text = `select count(location.line_num), bundesland.bundesland, bundesland.latitude, bundesland.longitude
				from (location join bundesland on location.bl_kz = bundesland.bl_Kz)
				group by bundesland.bl_kz order by count desc;`;
		}
		else if (req.body.regionLevel === 'bezirk') {
			q_text = `select count(location.line_num), bezirk.bezirk, bundesland.bundesland, bezirk.latitude, bezirk.longitude
				from ((location join bundesland on location.bl_kz = bundesland.bl_Kz) join bezirk on bezirk.bkz = location.bkz)
				group by bezirk.bkz, bundesland.bl_kz order by count desc;`;
		} else if (req.body.regionLevel === 'gemeinde') {
			q_text = `select count(location.line_num), gemeinde.gemeinde, bezirk.bezirk, bundesland.bundesland, gemeinde.latitude, gemeinde.longitude
				from (((location join bundesland on location.bl_kz = bundesland.bl_Kz)
				join bezirk on bezirk.bkz = location.bkz) join gemeinde on gemeinde.gkz = location.gkz)
				group by gemeinde.gkz, bezirk.bkz, bundesland.bl_kz order by count desc;`;
		} else if (req.body.regionLevel === 'ort') {
			q_text = `select count(location.line_num), ort.ort, gemeinde.gemeinde, bezirk.bezirk, bundesland.bundesland, ort.latitude,
				ort.longitude
				from ((((location join bundesland on location.bl_kz = bundesland.bl_Kz) join bezirk on bezirk.bkz = location.bkz)
				join gemeinde on gemeinde.gkz = location.gkz) join ort on ort.okz = location.okz)
				group by ort.okz, gemeinde.gkz, bezirk.bkz, bundesland.bl_kz order by count desc;`;
		} else {
			throw "unknown option for regionalView";
		}
		try {
			regionalData = await client.query(q_text)
			if (regionalData.rows.length == 0) {
				throw new Error('query empty');
			} else {
				console.log("in " + req.body.view + " result contains " + regionalData.rows.length + " items");
				res.send(regionalData.rows);
			}

			client.end()
			.then(() => console.log("disconnected"))
			.catch (err => console.log('disconnection error', err.stack));
		} catch (err) {
			console.log(err.message, err.stack)
			res.send(null);
		}
	} catch (err) {
		console.log("connection error", err.stack)
		res.send(null);
	}
});

app.post('/model_query', async (req, res) => {
	console.log("model_query request");

	const client = new Client({ connectionString: process.env.DATABASE_URL });
	let q_edges, q_intervals;
	try {
		await client.connect();
		console.log('connected');
		//q_intervals = `select count(bilanzindex) from eu_ratios group by bilanzindex order by bilanzindex;`
	} catch (err) {
		console.log(err.message, err.stack)
		res.send(null);
	}
	try {
		if(req.body.sources === undefined || req.body.sources.length == 0){
			console.log('No edges to display');
			res.send();
		}else{
			var params = [];
			var t = 3;
			for(var i = 0; i < req.body.sources.length; i++) {
				params.push('$' + t);
				t++;
			}
			q_edges = `SELECT source, target, weight FROM model_edge WHERE year_model = $1 AND id = $2 AND weight > 0 AND (source IN (`+params.join(",")+`) OR target IN (`+params.join(",")+`));`
			q_intervals = `SELECT MAX(weight), MIN(weight) FROM model_edge WHERE year_model = $1 AND id = $2 AND weight > 0 AND (source IN (`+params.join(",")+`) OR target IN (`+params.join(",")+`));`
			req.body.sources.unshift(req.body.model_id);
			req.body.sources.unshift(req.body.year);
			//console.log(req.body.sources);			
			let edges = await client.query(q_edges, req.body.sources);
			let intervals = await client.query(q_intervals, req.body.sources);
			if (edges.rows.length == 0) {
				throw new Error('query empty');
			} else {
				console.log("model_edges query contains " + edges.rows.length + " items");
				console.log("interval query contains " + intervals.rows.length + " items");                   

				res.send({
					edges: edges.rows,
					intervals: intervals.rows,
				});
			}
		}
	} catch (err) {
		console.log(err.message, err.stack)
		res.send(null);
	}
	finally{
		client.end()
		.then(() => console.log("disconnected"))
		.catch (err => console.log('disconnection error', err.stack));
	}
});

//not in use
app.post('/geo_sec_data', async (req, res) => {
//	console.log("geo_sec_data " + JSON.stringify(req.body));
	const client = new Client({ connectionString: process.env.DATABASE_URL });
	try {
		await client.connect();
		console.log('connected');
		let q_geo_sec_distr, adm_div_level;

		if (req.body.view === 'localView') {
			let ret = await client.query("select latitude, longitude from location where is_hq = 't';");
			if (ret.rows.length == 0) {
				throw new Error('query empty');
			} else {
				console.log("result contains " + ret.rows.length + " lines");
				res.send(ret.rows);
			}
		} else {
			if (req.body.view === 'regionalView') {
				if (req.body.regionLevel === 'bundesland') {
					adm_div_level = "bl_kz";
					q_geo_sec_distr = `select bundesland.bl_kz as kz, main_sec_distr_bl_kz.edv_e1, main_sec_distr_bl_kz.firm_count,
						bundesland.bundesland,
						bundesland.latitude, bundesland.longitude
						from (main_sec_distr_bl_kz join bundesland on main_sec_distr_bl_kz.bl_kz = bundesland.bl_Kz)
						order by bundesland.bl_kz, main_sec_distr_bl_kz.firm_count desc;`;
				}
				else if (req.body.regionLevel === 'bezirk') {
					adm_div_level = "bkz";
					q_geo_sec_distr = `select bezirk.bkz as kz, main_sec_distr_bkz.edv_e1, main_sec_distr_bkz.firm_count,
						bezirk.bezirk, bundesland.bundesland,
						bezirk.latitude, bezirk.longitude
						from ((bezirk join bundesland on bezirk.bl_kz = bundesland.bl_Kz)
						join main_sec_distr_bkz on bezirk.bkz = main_sec_distr_bkz.bkz)
						order by main_sec_distr_bkz.bkz, main_sec_distr_bkz.firm_count desc;`;
				} else if (req.body.regionLevel === 'gemeinde') {
					adm_div_level = "gkz";
					q_geo_sec_distr = `select gemeinde.gkz as kz, main_sec_distr_gkz.edv_e1, main_sec_distr_gkz.firm_count,
						gemeinde.gemeinde, bezirk.bezirk, bundesland.bundesland,
						gemeinde.latitude, gemeinde.longitude
						from (((gemeinde join bundesland on gemeinde.bl_kz = bundesland.bl_Kz)
						join bezirk on bezirk.bkz = gemeinde.bkz)
						join main_sec_distr_gkz on gemeinde.gkz = main_sec_distr_gkz.gkz)
						order by gemeinde.gkz, main_sec_distr_gkz.firm_count desc;`;
				} else if (req.body.regionLevel === 'ort') {
					adm_div_level = "okz";
					q_geo_sec_distr = `select ort.okz as kz, main_sec_distr_okz.edv_e1, main_sec_distr_okz.firm_count,
						ort.ort, gemeinde.gemeinde, bezirk.bezirk, bundesland.bundesland,
						ort.latitude, ort.longitude
						from ((((ort join bundesland on ort.bl_kz = bundesland.bl_Kz)
						join bezirk on bezirk.bkz = ort.bkz)
						join gemeinde on gemeinde.gkz = ort.gkz)
						join main_sec_distr_okz on ort.okz = main_sec_distr_okz.okz)
						order by ort.okz, main_sec_distr_okz.firm_count desc;`;
				} else {
					throw "unknown option for regionalView";
				}
				q_accu_sec_per_adm_div = `select ${adm_div_level} as kz, count(${adm_div_level}), sum(firm_count) as sum_firm_count
					from main_sec_distr_${adm_div_level}
					group by ${adm_div_level}
					order by sum_firm_count desc`;
			}
			else {
				throw "unknown view option";
			}
			try {
				let geo_sec_distr = await client.query(q_geo_sec_distr);
				let accu_sec_per_adm_div = await client.query(q_accu_sec_per_adm_div);

				num_rows = accu_sec_per_adm_div.rows.length;
				let max_sum_firm_count = accu_sec_per_adm_div.rows[0].sum_firm_count;
				let min_sum_firm_count = accu_sec_per_adm_div.rows[num_rows - 1].sum_firm_count;

				let {sec_info_in_adm_div} = accu_sec_per_adm_div.rows.reduce(
						(accu, curr) => {
							accu.sec_info_in_adm_div[curr.kz.toString()] = {beg: accu.accu_sec_count, size: curr.count, sum_firm_count: curr.sum_firm_count};
							accu.accu_sec_count += curr.count;
							return accu;
						},
						{sec_info_in_adm_div: {}, accu_sec_count: 0}
				);
				geo_sec_distr.rows.forEach(
						(row) => {
							row.sum_firm_count = sec_info_in_adm_div[row.kz.toString()].sum_firm_count;
						}
				);
				res.send({
					geo_sec_distr: geo_sec_distr.rows,
					max_sum_firm_count: max_sum_firm_count, // in each adm_div
					min_sum_firm_count: min_sum_firm_count,
					sec_info_in_adm_div: sec_info_in_adm_div
				});

				client.end()
				.then(() => console.log("disconnected"))
				.catch (err => console.log('disconnection error', err.stack));
			} catch (err) {
				console.log(err.message, err.stack)
				res.send(null);
			}
		}

	} catch (err) {
		console.log("connection error", err.stack)
		res.send(null);
	}
});
app.listen(port, () => console.log(`Listening on port ${port}`));
