# require python 3.4 or newer
from pathlib import Path
from datetime import date
from dotenv import load_dotenv
import psycopg2

import os
import csv
import traceback
import json
from psycopg2 import _psycopg
from psycopg2 import sql
import string
# abbr:
# bl_kz: Bundeslandkennziffer
# bkz:   Bezirks- oder Magistratskennziffer/code
# gkz:   Gemeindekennziffer/code
# okz:   Ortskennziffer

def sql_concat(delim, *strs):
    res = ""
    i = 0
    num_strs = len(strs)
    while i < num_strs - 1:
        res += strs[i] + delim
        i += 1
    return res + strs[i]

def exist_table(table_name, cur):
    sql_exist_table = "select to_regclass('public." + table_name + "');"
    cur.execute(sql_exist_table)
    # cur.rowcount should always be 1
    if cur.rowcount != 1:
        raise AssertionError("select to_regclass returned nothing or more than one row")
    else:
        curr_row = cur.fetchone()
        if curr_row[0] != None:
            return True
        else:
            return False

def create_table_bundesland(src_csv_file, db_info):
    BUNDESLANDKENNZIFFER = 0
    BUNDESLAND = 1
    LANDESHAUPTSTADT = 2
    LATITUDE = 3
    LONGITUDE = 4
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "bundesland"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            conn.close()
            print ("disconnected from database smartct...\n")
            return

        name0 = "bl_kz"
        type0 = "integer"
        opt0 = "primary key"
        col0 = sql_concat(' ', name0, type0, opt0)

        name1 = "bundesland"
        type1 = "varchar"
        col1 = sql_concat(' ', name1, type1)

        name2 = "landeshauptstadt"
        type2 = "varchar"
        col2 = sql_concat(' ', name2, type2)

        name3 = "latitude"
        type3 = "varchar"
        col3 = sql_concat(' ', name3, type3)

        name4 = "longitude"
        type4 = "varchar"
        col4 = sql_concat(' ', name4, type4)

        sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3, col4) + ");"
        cur.execute(sql_create_table)
        print ("sql_create_table:" + sql_create_table)

        with open(src_csv_file, newline='', encoding='utf-8') as src_csv_file:
            src_csv = csv.reader(src_csv_file, delimiter=';')
            print(next(src_csv)) # skip header
            num_import_rows = 0
            while True:
                try:
                    src_row = next(src_csv)
                    bl_kz = str(src_row[BUNDESLANDKENNZIFFER])
                    bl = str(src_row[BUNDESLAND])
                    lhs = str(src_row[LANDESHAUPTSTADT])
                    lat = str(round(float(src_row[LATITUDE]), 7))
                    lng = str(round(float(src_row[LONGITUDE]), 7))
                    sql_ins = "insert into " + table_name + " values (" + "%s, %s, %s, %s, %s" + ");"
                    data = (bl_kz, bl, lhs, lat, lng)
                    cur.execute(sql_ins, data)
                    num_import_rows += 1
                except StopIteration as e:
                    print ("finished importing {0} number of rows into {1}".format(num_import_rows, table_name))
                    break
        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")
    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_database(db_name, db_host, db_info):
    print("checking database")
    try:
        conn = psycopg2.connect(db_info)
        conn.autocommit = True
        print ("connected to the database...")
        cur = conn.cursor()
        cur.execute("SELECT datname FROM pg_database")
        rows = cur.fetchall()
        found = False
        for row in rows:
            if row[0] == db_name:
                found = True
                print("Database exists!")
                break
        if found == False:
            try:
                cur.execute(
                    sql.SQL("CREATE DATABASE {} OWNER {}".format(db_name, db_host)))
                conn.commit()
                print("Database created succesfully")
            except Exception as E:
                print(E)
                print("Count not create the database")
                conn.rollback()
            finally:
                conn.close()
        else:
            print("Found appears to be there")
    except:
        print ("Connection to database failed")


def create_table_bezirk(src_csv_file, db_info):
    BEZIRKSKENNZIFFER = 0
    BEZIRK = 1
    BUNDESLANDKENNZIFFER = 2
    LATITUDE = 3
    LONGITUDE = 4
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "bezirk"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            conn.close()
            print ("disconnected from database smartct...\n")
            return

        name0 = "bkz"
        type0 = "integer"
        opt0 = "primary key"
        col0 = sql_concat(' ', name0, type0, opt0)

        name1 = "bezirk"
        type1 = "varchar"
        col1 = sql_concat(' ', name1, type1)

        name2 = "bl_kz"
        type2 = "integer"
        col2 = sql_concat(' ', name2, type2)

        name3 = "latitude"
        type3 = "varchar"
        col3 = sql_concat(' ', name3, type3)

        name4 = "longitude"
        type4 = "varchar"
        col4 = sql_concat(' ', name4, type4)

        sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3, col4) + ");"
        cur.execute(sql_create_table)
        print ("sql_create_table:" + sql_create_table)

        with open(src_csv_file, newline='', encoding='utf-8') as src_csv_file:
            src_csv = csv.reader(src_csv_file, delimiter=';')
            print(next(src_csv)) # skip header
            num_import_rows = 0
            while True:
                try:
                    src_row = next(src_csv)
                    bkz = str(src_row[BEZIRKSKENNZIFFER])
                    bk = str(src_row[BEZIRK])
                    bl_kz = str(src_row[BUNDESLANDKENNZIFFER])
                    lat = str(round(float(src_row[LATITUDE]), 7))
                    lng = str(round(float(src_row[LONGITUDE]), 7))
                    sql_ins = "insert into " + table_name + " values (" + "%s, %s, %s, %s, %s" + ");"
                    data = (bkz, bk, bl_kz, lat, lng)
                    cur.execute(sql_ins, data)
                    num_import_rows += 1
                except StopIteration as e:
                    print ("finished importing {0} number of rows into {1}".format(num_import_rows, table_name))
                    break
        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")
    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_table_gemeinde(src_csv_file, db_info):
    GEMEINDESKENNZIFFER = 0
    GEMEINDE = 1
    BEZIRKSKENNZIFFER = 2
    BUNDESLANDKENNZIFFER = 3
    LATITUDE = 4
    LONGITUDE = 5
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "gemeinde"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            conn.close()
            print ("disconnected from database smartct...\n")
            return

        name0 = "gkz"
        type0 = "integer"
        opt0 = "primary key"
        col0 = sql_concat(' ', name0, type0, opt0)

        name1 = "gemeinde"
        type1 = "varchar"
        col1 = sql_concat(' ', name1, type1)

        name2 = "bkz"
        type2 = "integer"
        col2 = sql_concat(' ', name2, type2)

        name3 = "bl_kz"
        type3 = "integer"
        col3 = sql_concat(' ', name3, type3)

        name4 = "latitude"
        type4 = "varchar"
        col4 = sql_concat(' ', name4, type4)

        name5 = "longitude"
        type5 = "varchar"
        col5 = sql_concat(' ', name5, type5)

        sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3, col4, col5) + ");"
        cur.execute(sql_create_table)
        print ("sql_create_table:" + sql_create_table)

        with open(src_csv_file, newline='', encoding='utf-8') as src_csv_file:
            src_csv = csv.reader(src_csv_file, delimiter=';')
            print(next(src_csv)) # skip header
            num_import_rows = 0
            while True:
                try:
                    src_row = next(src_csv)
                    gkz = str(src_row[GEMEINDESKENNZIFFER])
                    gd = str(src_row[GEMEINDE])
                    bkz = str(src_row[BEZIRKSKENNZIFFER])
                    bl_kz = str(src_row[BUNDESLANDKENNZIFFER])
                    lat = str(round(float(src_row[LATITUDE]), 7))
                    lng = str(round(float(src_row[LONGITUDE]), 7))
                    sql_ins = "insert into " + table_name + " values (" + "%s, %s, %s, %s, %s, %s" + ");"
                    data = (gkz, gd, bkz, bl_kz, lat, lng)
                    cur.execute(sql_ins, data)
                    num_import_rows += 1
                except StopIteration as e:
                    print ("finished importing {0} number of rows into {1}".format(num_import_rows, table_name))
                    break
        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")
    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_table_ort(src_csv_file, db_info):
    ORTSKENNZIFFER = 0
    ORT = 1
    GEMEINDESKENNZIFFER = 2
    BEZIRKSKENNZIFFER = 3
    LATITUDE = 4
    LONGITUDE = 5
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "ort"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            conn.close()
            print ("disconnected from database smartct...\n")
            return

        name0 = "okz"
        type0 = "integer"
        opt0 = "primary key"
        col0 = sql_concat(' ', name0, type0, opt0)

        name1 = "ort"
        type1 = "varchar"
        col1 = sql_concat(' ', name1, type1)

        name2 = "gkz"
        type2 = "integer"
        col2 = sql_concat(' ', name2, type2)

        name3 = "bkz"
        type3 = "integer"
        col3 = sql_concat(' ', name3, type3)

        name4 = "bl_kz"
        type4 = "integer"
        col4 = sql_concat(' ', name4, type4)

        name5 = "latitude"
        type5 = "varchar"
        col5 = sql_concat(' ', name5, type5)

        name6 = "longitude"
        type6 = "varchar"
        col6 = sql_concat(' ', name6, type6)

        sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3, col4, col5, col6) + ");"
        cur.execute(sql_create_table)
        print ("sql_create_table:" + sql_create_table)

        with open(src_csv_file, newline='', encoding='utf-8') as src_csv_file:
            src_csv = csv.reader(src_csv_file, delimiter=';')
            print(next(src_csv)) # skip header
            num_import_rows = 0
            while True:
                try:
                    src_row = next(src_csv)
                    okz = str(src_row[ORTSKENNZIFFER])
                    ort = str(src_row[ORT])
                    gkz = str(src_row[GEMEINDESKENNZIFFER])
                    bkz = str(src_row[BEZIRKSKENNZIFFER])
                    bl_kz = gkz[0]
                    lat = str(round(float(src_row[LATITUDE]), 7))
                    lng = str(round(float(src_row[LONGITUDE]), 7))
                    sql_ins = "insert into " + table_name + " values (" + "%s, %s, %s, %s, %s, %s, %s" + ");"
                    data = (okz, ort, gkz, bkz, bl_kz, lat, lng)
                    cur.execute(sql_ins, data)
                    num_import_rows += 1
                except StopIteration as e:
                    print ("finished importing {0} number of rows into {1}".format(num_import_rows, table_name))
                    break
        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")
    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_table_plz(src_csv_file, db_info):
    PLZ = 0
    ORTSKENNZIFFER = 2
    ORT = 3
    GEMEINDEKENNZIFFER = 4
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "plz"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            conn.close()
            print ("disconnected from database smartct...\n")
            return

        name0 = "plz"
        type0 = "integer"
        col0 = sql_concat(' ', name0, type0)

        name1 = "okz"
        type1 = "integer"
        opt1 = ", primary key (" + sql_concat(', ', name0, name1) + ")"
        col1 = sql_concat(' ', name1, type1, opt1)

        name2 = "ort"
        type2 = "varchar"
        col2 = sql_concat(' ', name2, type2)

        name3 = "gkz"
        type3 = "integer"
        col3 = sql_concat(' ', name3, type3)

        name4 = "bkz"
        type4 = "integer"
        col4 = sql_concat(' ', name4, type4)

        name5 = "bl_kz"
        type5 = "integer"
        col5 = sql_concat(' ', name5, type5)

        sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3, col4, col5) + ");"
        cur.execute(sql_create_table)
        print ("sql_create_table:" + sql_create_table)

        with open(src_csv_file, newline='', encoding='utf-8') as src_csv_file:
            src_csv = csv.reader(src_csv_file, delimiter=',')
            print(next(src_csv)) # skip header
            num_import_rows = 0
            while True:
                try:
                    src_row = next(src_csv)
                    okz = str(src_row[ORTSKENNZIFFER])
                    ort = str(src_row[ORT])
                    plz = str(src_row[PLZ])
                    gkz = str(src_row[GEMEINDEKENNZIFFER])
                    bkz = gkz[:3]
                    bl_kz = gkz[0]
                    sql_ins = "insert into " + table_name + " values (" + "%s, %s, %s, %s, %s, %s" + ");"
                    data = (plz, okz, ort, gkz, bkz, bl_kz)
                    cur.execute(sql_ins, data)
                    num_import_rows += 1
                except StopIteration as e:
                    print ("finished importing {0} number of rows into {1}".format(num_import_rows, table_name))
                    break
        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")
    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_table_oenace(src_csv_file, lang, db_info):
    EBENE = 0
    EDV_CODE = 1
    CODE = 2
    TITEL = 3
    KURZTITEL = 4
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "oenace2008" + lang

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            conn.close()
            print ("disconnected from database smartct...\n")
            return

        name0 = "ebene"
        type0 = "integer"
        col0 = sql_concat(' ', name0, type0)

        name1 = "edv_code"
        type1 = "varchar"
        opt1 = "primary key"
        col1 = sql_concat(' ', name1, type1, opt1)

        name2 = "code"
        type2 = "varchar"
        col2 = sql_concat(' ', name2, type2)

        name3 = "titel"
        type3 = "varchar"
        col3 = sql_concat(' ', name3, type3)

        name4 = "kurztitel"
        type4 = "varchar"
        col4 = sql_concat(' ', name4, type4)

        sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3, col4) + ");"
        cur.execute(sql_create_table)
        print ("sql_create_table:" + sql_create_table)

        with open(src_csv_file, newline='', encoding='utf-8') as src_csv_file:
            src_csv = csv.reader(src_csv_file, delimiter=';')
            print(next(src_csv)) # skip header
            num_import_rows = 0
            while True:
                try:
                    src_row = next(src_csv)
                    ebene = str(src_row[EBENE])
                    edv_code = str(src_row[EDV_CODE])
                    code = str(src_row[CODE])
                    titel = str(src_row[TITEL])
                    if lang == 'en':
                        kurztitel = titel
                    else:
                        kurztitel = str(src_row[KURZTITEL])
                    sql_ins = "insert into " + table_name + " values (" + "%s, %s, %s, %s, %s" + ");"
                    data = (ebene, edv_code, code, titel, kurztitel)
                    cur.execute(sql_ins, data)
                    num_import_rows += 1
                except StopIteration as e:
                    print ("finished importing {0} number of rows into {1}".format(num_import_rows, table_name))
                    break
        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")
    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_table_sector(src_csv_file, db_info):
    ID = 0
    EDV_CODE = 1
    MAIN_ACTIVITY = 2
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "sector"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            conn.close()
            print ("disconnected from database smartct...\n")
            return

        name0 = "firm_id"
        type0 = "integer"
        col0 = sql_concat(' ', name0, type0)

        name1 = "edv_code"
        type1 = "char(6)"
        opt1 = ", primary key (" + sql_concat(', ', name0, name1) + ")"
        col1 = sql_concat(' ', name1, type1, opt1)

        name2 = "is_main"
        type2 = "boolean"
        col2 = sql_concat(' ', name2, type2)

        name3 = "edv_e1"
        type3 = "char(1)"
        col3 = sql_concat(' ', name3, type3)

        name4 = "edv_e2"
        type4 = "char(3)"
        col4 = sql_concat(' ', name4, type4)

        name5 = "edv_e3"
        type5 = "char(4)"
        col5 = sql_concat(' ', name5, type5)

        name6 = "edv_e4"
        type6 = "char(5)"
        col6 = sql_concat(' ', name6, type6)

        sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3, col4, col5, col6) + ");"
        cur.execute(sql_create_table)
        print ("sql_create_table:" + sql_create_table)

        with open(src_csv_file, newline='', encoding='utf-8') as src_csv_file:
            src_csv = csv.reader(src_csv_file, delimiter=';')
            print(next(src_csv)) # skip header
            num_import_rows = 0
            while True:
                try:
                    src_row = next(src_csv)
                    firm_id = str(src_row[ID])
                    edv_code = str(src_row[EDV_CODE])
                    is_main = str(src_row[MAIN_ACTIVITY])
                    edv_e1 = edv_code[0]
                    edv_e2 = edv_code[0:3]
                    edv_e3 = edv_code[0:4]
                    edv_e4 = edv_code[0:5]
                    sql_ins = "insert into " + table_name + " values (" + "%s, %s, %s, %s, %s, %s, %s" + ");"
                    data = (firm_id, edv_code, is_main, edv_e1, edv_e2, edv_e3, edv_e4)
                    cur.execute(sql_ins, data)
                    num_import_rows += 1
                except StopIteration as e:
                    print ("finished importing {0} number of rows into {1}".format(num_import_rows, table_name))
                    break
        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")
    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_table_location_without_hq_info(src_csv_file, db_info):
    LINE_NUM = 0
    FIRM_ID = 1
    STREET = 2
    PLZ = 3
    ORTSKENNZIFFER = 4
    GEMEINDEKENNZIFFER = 5
    LATITUDE = 8
    LONGITUDE = 9
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "location_without_hq_info"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            conn.close()
            print ("disconnected from database smartct...\n")
            return

        name0 = "line_num"
        type0 = "integer"
        opt0 = "primary key"
        col0 = sql_concat(' ', name0, type0, opt0)

        name1 = "firm_id"
        type1 = "integer"
        col1 = sql_concat(' ', name1, type1)

        name2 = "street"
        type2 = "varchar"
        col2 = sql_concat(' ', name2, type2)

        name3 = "plz"
        type3 = "integer"
        col3 = sql_concat(' ', name3, type3)

        name4 = "okz"
        type4 = "integer"
        col4 = sql_concat(' ', name4, type4)

        name5 = "gkz"
        type5 = "integer"
        col5 = sql_concat(' ', name5, type5)

        name6 = "bkz"
        type6 = "integer"
        col6 = sql_concat(' ', name6, type6)

        name7 = "bl_kz"
        type7 = "integer"
        col7 = sql_concat(' ', name7, type7)

        name8 = "latitude"
        type8 = "varchar"
        col8 = sql_concat(' ', name8, type8)

        name9 = "longitude"
        type9 = "varchar"
        col9 = sql_concat(' ', name9, type9)

        sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3, col4, col5, col6, col7, col8, col9) + ");"
        cur.execute(sql_create_table)
        print ("sql_create_table:" + sql_create_table)

        with open(src_csv_file, newline='', encoding='utf-8') as src_csv_file:
            src_csv = csv.reader(src_csv_file, delimiter=';')
            print(next(src_csv)) # skip header
            num_import_rows = 0
            while True:
                try:
                    src_row = next(src_csv)
                    line_num = str(src_row[LINE_NUM])
                    firm_id = str(src_row[FIRM_ID])
                    street = str(src_row[STREET])
                    plz = str(src_row[PLZ])
                    okz = str(src_row[ORTSKENNZIFFER])
                    gkz = str(src_row[GEMEINDEKENNZIFFER])
                    bkz = gkz[:3]
                    bl_kz = gkz[0]
                    lat = str(src_row[LATITUDE])
                    lng = str(src_row[LONGITUDE])
                    sql_ins = "insert into " + table_name + " values (" + "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s" + ");"
                    data = (line_num, firm_id, street, plz, okz, gkz, bkz, bl_kz, lat, lng)
                    cur.execute(sql_ins, data)
                    num_import_rows += 1
                except StopIteration as e:
                    print ("finished importing {0} number of rows into {1}".format(num_import_rows, table_name))
                    break
        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")
    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_table_location(src_csv_file, db_info):
    LINE_NUM = 0
    FIRM_ID = 1
    STREET = 2
    PLZ = 3
    ORTSKENNZIFFER = 4
    GEMEINDEKENNZIFFER = 5
    LATITUDE = 8
    LONGITUDE = 9
    IS_HQ = 10
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "location"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            conn.close()
            print ("disconnected from database smartct...\n")
            return


        name0 = "line_num"
        type0 = "integer"
        opt0 = "primary key"
        col0 = sql_concat(' ', name0, type0, opt0)

        name1 = "firm_id"
        type1 = "integer"
        col1 = sql_concat(' ', name1, type1)

        name2 = "street"
        type2 = "varchar"
        col2 = sql_concat(' ', name2, type2)

        name3 = "plz"
        type3 = "integer"
        col3 = sql_concat(' ', name3, type3)

        name4 = "okz"
        type4 = "integer"
        col4 = sql_concat(' ', name4, type4)

        name5 = "gkz"
        type5 = "integer"
        col5 = sql_concat(' ', name5, type5)

        name6 = "bkz"
        type6 = "integer"
        col6 = sql_concat(' ', name6, type6)

        name7 = "bl_kz"
        type7 = "integer"
        col7 = sql_concat(' ', name7, type7)

        name8 = "latitude"
        type8 = "varchar"
        col8 = sql_concat(' ', name8, type8)

        name9 = "longitude"
        type9 = "varchar"
        col9 = sql_concat(' ', name9, type9)

        name10 = "is_hq"
        type10 = "boolean"
        col10 = sql_concat(' ', name10, type10)

        sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10) + ");"
        cur.execute(sql_create_table)
        print ("sql_create_table:" + sql_create_table)

        with open(src_csv_file, newline='', encoding='utf-8') as src_csv_file:
            src_csv = csv.reader(src_csv_file, delimiter=';')
            print(next(src_csv)) # skip header
            num_import_rows = 0
            while True:
                try:
                    src_row = next(src_csv)
                    line_num = str(src_row[LINE_NUM])
                    firm_id = str(src_row[FIRM_ID])
                    street = str(src_row[STREET])
                    plz = str(src_row[PLZ])
                    okz = str(src_row[ORTSKENNZIFFER])
                    gkz = str(src_row[GEMEINDEKENNZIFFER])
                    bkz = gkz[:3]
                    bl_kz = gkz[0]
                    lat = str(src_row[LATITUDE])
                    lng = str(src_row[LONGITUDE])
                    is_hq = str(src_row[IS_HQ])
                    sql_ins = "insert into " + table_name + " values (" + "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s" + ");"
                    data = (line_num, firm_id, street, plz, okz, gkz, bkz, bl_kz, lat, lng, is_hq)
                    cur.execute(sql_ins, data)
                    num_import_rows += 1
                except StopIteration as e:
                    print ("finished importing {0} number of rows into {1}".format(num_import_rows, table_name))
                    break
        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")
    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_tables_sec_distr():
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()

        adm_divs = ["bl_kz", "bkz", "gkz", "okz"]

        for adm_div in adm_divs:
            table_name = "main_sec_distr_" + adm_div

            if exist_table(table_name, cur):
                print ("warning: table {0} already exists".format(table_name))
                continue

            name0 = adm_div
            type0 = "integer"
            col0 = sql_concat(' ', name0, type0)

            name1 = "edv_e1"
            type1 = "char(1)"
            opt1 = ", primary key (" + sql_concat(', ', name0, name1) + ")"
            col1 = sql_concat(' ', name1, type1, opt1)

            name2 = "firm_count"
            type2 = "integer"
            col2 = sql_concat(' ', name2, type2)

            sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2) + ");"
            cur.execute(sql_create_table)
            print ("sql_create_table:" + sql_create_table)

            sql_ins = "insert into " + table_name + " (" + "select location."+ adm_div + ", sector.edv_e1, count(location.line_num) as firm_count from location cross join sector where location.firm_id = sector.firm_id and location.is_hq = 't'and sector.is_main = 't' group by location." + adm_div + ", sector.edv_e1 order by location." + adm_div + " asc, firm_count desc" + ");"
            cur.execute(sql_ins)

        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")

    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_table_eu_ratios(src_csv_file, db_info):
    ID = 0
    BILANZSTICHTAG = 1
    CASHFLOW = 2
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "eu_ratios"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            conn.close()
            print ("disconnected from database smartct...\n")
            return

        name0 = "firm_id"
        type0 = "integer"
        col0 = sql_concat(' ', name0, type0)

        name1 = "bilanzstichtag"
        type1 = "date"
        opt1 = ", primary key (" + sql_concat(', ', name0, name1) + ")"
        col1 = sql_concat(' ', name1, type1, opt1)

        # bilanzindex:
        # 0: report in interval [2006-04-01, 2007-03-31]; 1: report in interval [2007-04-01, 2008-03-31]...
        name2 = "bilanzindex"
        type2 = "integer"
        col2 = sql_concat(' ', name2, type2)

        name3 = "cash_flow"
        type3 = "float"
        col3 = sql_concat(' ', name3, type3)

        sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3) + ");"
        cur.execute(sql_create_table)
        conn.commit()
        print ("sql_create_table:" + sql_create_table)

        with open(src_csv_file, newline='', encoding='utf-8') as src_csv_file:
            src_csv = csv.reader(src_csv_file, delimiter=';')
            print(next(src_csv)) # skip header
            num_import_rows = 0
            while True:
                try:
                    src_row = next(src_csv)
                    firm_id = str(src_row[ID])
                    bilanzstichtag = str(src_row[BILANZSTICHTAG])
                    cash_flow = str(src_row[CASHFLOW])

                    if firm_id == "" or bilanzstichtag == "":
                        continue

                    #reportDay = datetime.date.fromisoformat(bilanzstichtag)
                    explosion = bilanzstichtag.split("-")
                    reportDay = date(int(explosion[0]), int(explosion[1]), int(explosion[2]))
                    reportYear = reportDay.year
                    if reportDay.month < 4:
                        bilanzindex = reportYear - 2007
                    else:
                        bilanzindex = reportYear + 1 - 2007

                    sql_ins = "insert into " + table_name + " values (" + "%s, %s, %s, %s" + ");"
                    data = (firm_id, bilanzstichtag, bilanzindex, cash_flow)
                    try:
                        cur.execute(sql_ins, data)
                        conn.commit()
                    except psycopg2.IntegrityError:
                        conn.commit()
                        cur.execute("select cash_flow from " + table_name + " where firm_id = %s and bilanzstichtag = %s;", (int(firm_id), reportDay))#date.fromisoformat(bilanzstichtag)))
                        if cur.rowcount != 1:
                            print("{} duplicate key(id, bilanzstichtag)=({},{})", cur.rowcount, firm_id, bilanzstichtag)
                        else:
                            exist_cash_flow = cur.fetchone()[0]
                            if exist_cash_flow == "":
                                print("delete existing record (id, bilanzstichtag)=({},{})", firm_id, bilanzstichtag)
                                cur.execute("delete from " + table_name + " where firm_id = %s and bilanzstichtag = %s;", (int(firm_id), reportDay))#date.fromisoformat(bilanzstichtag)))
                                conn.commit()
                                cur.execute(sql_ins, data)
                                conn.commit()
                            else:
                                print("ignore current record (id, bilanzstichtag, cash_flow)=({},{}, {})", firm_id, bilanzstichtag, cash_flow)

                    num_import_rows += 1
                except StopIteration as e:
                    print ("finished importing {0} number of rows into {1}".format(num_import_rows, table_name))
                    break
        conn.commit()
        conn.close()
        print ("disconnected from database smartct...\n")
    except Exception as e:
        print ("Error: " + str(e))
        print (traceback.format_exc())
        if conn.closed == 0:
            conn.close()
            print ("disconnected from database smartct...\n")

def create_table_edges(edges_json_directory, db_info):
    try:
        conn = psycopg2.connect(db_info)
        print ("connected to the database...")
        cur = conn.cursor()
        table_name = "model_labels"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            #conn.close()
            #print ("disconnected from database smartct...\n")
            #return
        else:
            name0 = "id"
            type0 = "SERIAL"
            opt0 = "PRIMARY KEY"
            col0 = sql_concat(' ', name0, type0, opt0)

            name1 = "label"
            type1 = "VARCHAR(100)"
            col1 = sql_concat(' ', name1, type1)

            sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1) + ");"
            cur.execute(sql_create_table)
            conn.commit()
            print ("sql_create_table:" + sql_create_table)
            print("Table created successfully")

        table_name_labels = table_name
        table_name = "model_edge"

        if exist_table(table_name, cur):
            print ("warning: table {0} already exists".format(table_name))
            #conn.close()
            #print ("disconnected from database smartct...\n")
            #return
        else:
            name0 = "id"
            type0 = "INT"
            opt0 = "REFERENCES model_labels(id)"
            col0 = sql_concat(' ', name0, type0, opt0)

            name1 = "year_model"
            type1 = "integer"
            col1 = sql_concat(' ', name1, type1)

            name2 = "source"
            type2 = "integer"
            col2 = sql_concat(' ', name2, type2)

            name3 = "target"
            type3 = "integer"
            col3 = sql_concat(' ', name3, type3)

            # bilanzindex:
            # 0: report in interval [2006-04-01, 2007-03-31]; 1: report in interval [2007-04-01, 2008-03-31]...
            name4 = "weight"
            type4 = "float"
            col4 = sql_concat(' ', name4, type4)

            #name3 = "cash_flow"
            #type3 = "float"
            #col3 = sql_concat(' ', name3, type3)

            opt0 = "primary key ("+sql_concat(",", name0, name1, name2, name3)+")"

            sql_create_table = "create table public." + table_name + " (" + sql_concat(", ", col0, col1, col2, col3, col4) + ");"
            cur.execute(sql_create_table)
            conn.commit()
            print ("sql_create_table:" + sql_create_table)
            print("Table created successfully")

        sql_ins_edge = "insert into " + table_name + " values (" + "%s, %s, %s, %s, %s" + ");"
        sql_ins_model = "insert into " + table_name_labels + "(label) values (" + "%s" + ") RETURNING id;"
        sql_src_model = "select id from " + table_name_labels + " where label = %s;"
        counter = 0
        for edges_json_subdir in os.listdir(edges_json_directory):
            model_name = os.path.basename(edges_json_subdir)

            cur.execute(sql_src_model, [model_name])
            row = cur.fetchone()
            if(row is not None):
                print("Model " + model_name + " already exists")
                continue
            cur.execute(sql_ins_model, [model_name])
            row = cur.fetchone()
            model_id = row[0]
            print("Recorded model " + model_name)
            subpath = os.path.join(edges_json_directory, edges_json_subdir)
            for edges_json_file in os.listdir(subpath):
                year = os.path.splitext(edges_json_file)[0];
                print("Importing year " + year)
                with open(os.path.join(subpath, edges_json_file)) as json_file:
                    json_data = json.load(json_file)
                    for key, value in json_data.items():
                        i = 0
                        for activity in value["outflows"]:
                            #print(activity)
                            data = (model_id, year, value["sabina_id"], json_data[str(i)]["sabina_id"], activity)
                            #print(data)
                            cur.execute(sql_ins_edge, data)
                            counter += 1
                            i += 1
            conn.commit()
        print("Added " + str(counter) + " lines to table")
    except Exception as e:
        conn.rollback()
        print(e)
    finally:
        conn.close()
        print("Data succesfully added")
        print("Disconnected from Database")


os.chdir("..")
env_path = os.path.join(os.getcwd(),"db.env")
load_dotenv(dotenv_path=env_path)
db_name = os.getenv("DB_NAME")
db_username = os.getenv("DB_USERNAME")
db_password = os.getenv("DB_PASSWORD")
db_host = os.getenv("DB_HOST")
db_port = os.getenv("DB_PORT")
db_info = "dbname={} user={} password={} host={} port={}".format(db_name, db_username, db_password, db_host, db_port)
db_info_nodb = "dbname=postgres user={} password={} host={} port={}".format(db_username, db_password, db_host, db_port)
# create_table_location_without_hq_info("./geo_data/location_without_hq_info.csv", db_info)
create_database(db_name, db_username, db_info_nodb)

os.chdir("py")

create_table_bundesland("geo_data/lat_lng_bundesland.csv", db_info)
create_table_bezirk("./geo_data/lat_lng_bezirk.csv", db_info)
create_table_gemeinde("./geo_data/lat_lng_gemeinde.csv", db_info)
create_table_ort("./geo_data/lat_lng_ort.csv", db_info)
create_table_plz("./geo_data/plz.csv", db_info)

create_table_location("./geo_data/location.csv", db_info)
create_table_oenace("./geo_data/oenace2008de.csv", "de", db_info)
create_table_oenace("./geo_data/oenace2008en.csv", "en", db_info)
create_table_sector("./geo_data/sector.csv", db_info)

create_table_edges("./geo_data/edges", db_info)

create_table_eu_ratios("./geo_data/eu_ratios.csv", db_info)
create_tables_sec_distr()
