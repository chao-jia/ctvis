import {Layer, CompositeLayer, HexagonLayer, ArcLayer, HexagonCellLayer, WebMercatorViewport} from 'deck.gl';
import {hexbin} from 'd3-hexbin';
import React from "react";
import BinSorter from './bin-sorter';
import {ArcLauncher} from './arc-launcher';
import RcSlider from 'rc-slider';
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';

import {withStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Slider from '@material-ui/lab/Slider';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Fab from '@material-ui/core/Fab';
import Paper from '@material-ui/core/Paper';
import Collapse from '@material-ui/core/Collapse';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Tooltip from '@material-ui/core/Tooltip'

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

import { Settings, SettingsOutline } from 'mdi-material-ui';

import ColorLegend from './ui_overlays';

const MAPBOX_DEFAULT_MAPSTYLES = [
	{
		label: 'Streets V10',
		value: 'mapbox://styles/mapbox/streets-v10'
	},
	{
		label: 'Outdoors V10',
		value: 'mapbox://styles/mapbox/outdoors-v10'
	},
	{
		label: 'Light V9',
		value: 'mapbox://styles/mapbox/light-v9'
	},
	{
		label: 'Dark V9',
		value: 'mapbox://styles/mapbox/dark-v9'
	},
	{
		label: 'Satellite V9',
		value: 'mapbox://styles/mapbox/satellite-v9'
	},
	{
		label: 'Satellite Streets V10',
		value: 'mapbox://styles/mapbox/satellite-streets-v10'
	},
	{
		label: 'Navigation Preview Day V4',
		value: 'mapbox://styles/mapbox/navigation-preview-day-v4'
	},
	{
		label: 'Navitation Preview Night  V4',
		value: 'mapbox://styles/mapbox/navigation-preview-night-v4'
	},
	{
		label: 'Navigation Guidance Day V4',
		value: 'mapbox://styles/mapbox/navigation-guidance-day-v4'
	},
	{
		label: 'Navigation Guidance Night V4',
		value: 'mapbox://styles/mapbox/navigation-guidance-night-v4'
	}
	];

const HEATMAP_COLORS = [
	[69,117,180],
	[145,191,219],
	[224,243,248],
	[254,224,144],
	[252,141,89],
	[215,48,39]
	];

const NEGATIVE_COLORS = [
	[252,187,161],
	[252,146,114],
	[251,106,74],
	[239,59,44],
	[203,24,29],
	[153,0,13]
	];

const POSITIVE_COLORS = [
	[198,219,239],
	[158,202,225],
	[107,174,214],
	[66,146,198],
	[33,113,181],
	[8,69,148]
	];

const LOCAL_ARC_WIDTH_RANGE = [12,12];

function linearMapping(domain, range, s) {
	return (range[1] - range[0]) * (s - domain[0]) / (domain[1] - domain[0]) + range[0];
}

function lerp(a, b, t) {
	return a * (1 - t) + b * t;
}

function clamp(c, a, b) {
	return (c < a) ? a : ((c > b) ? b : c);
}

function colorMapping(colorRange, s, sMin, sMax) {
	const numLevels = colorRange.length;
	const stepSize = (sMax - sMin) / (numLevels - 1);
	const rangeIdx = clamp(Math.floor((s - sMin) / stepSize), 0, numLevels - 2);
	const t = (s - sMin) / stepSize - rangeIdx;
	const colorFrom = colorRange[rangeIdx];
	const colorTo = colorRange[rangeIdx + 1];
	return colorFrom.map(
			(value, index) => {
				return Math.round(lerp(value, colorTo[index], t));
			}
	);
}

function quantizeScale(domain, range, value) {
	const domainRange = domain[1] - domain[0];
	if (domainRange <= 0) {
		console.log('quantizeScale: invalid domain, returning range[0] (' + domain[1]  + " - " + domain[0] + ")");
		return range[0];
	}
	const step = domainRange / range.length;
	const idx = Math.floor((value - domain[0]) / step);
	const clampIdx = Math.max(Math.min(idx, range.length - 1), 0);

	return range[clampIdx];
}

const LIGHT_SETTINGS = {
		lightsPosition: [9, 46, 30000, 18, 49, 30000],
		ambientRatio: 0.3,
		diffuseRatio: 0.8,
		specularRatio: 0.2,
		lightsStrength: [0.8, 0.8, 0.8, 0.8],
		numberOfLights: 2
};

const localElevationRange = [100, 1000000];
const regionalElevationRange = [100, 200000];

export function getRadiusInPixel(radius, viewport) {
	const {pixelsPerMeter} = viewport.getDistanceScales();

	// x, y distance should be the same
	return radius * pixelsPerMeter[0];
}

function pointToHexbinFixedCenter({data, firmsYearlys, euRatiosYearlys, radius, getPosition}, viewportIn) {
	// (47.5, 13.5) center of austria
	let viewport = new WebMercatorViewport({zoom: viewportIn.zoom, longitude: 13.5, latitude: 47.5});
	// get hexagon radius in mercator world unit
	const radiusInPixel = getRadiusInPixel(radius, viewport);
	// add world space coordinates to points
	const screenPoints = [];
	for (const pt of data) {
		screenPoints.push(
				Object.assign(
						{
							screenCoord: viewport.projectFlat(getPosition(pt))
						},
						pt
				)
		);
	}

	const newHexbin = hexbin()
	.radius(radiusInPixel)
	.x(d => d.screenCoord[0])
	.y(d => d.screenCoord[1]);

	const hexagonBins = newHexbin(screenPoints);

	let hexagons = hexagonBins.map((hex, index) => {
		let {numFirmsYearlysInBin, euRatiosYearlysInBin} = firmsYearlys.reduce(
				(accu, firmsYearly, idx) => {
					const firmsCurrYearInBin = hex.filter(firm => {return firmsYearly.has(firm.firm_id)});
					const euRatiosYearly = euRatiosYearlys[idx];
					const euRatiosCurrYearInBin = firmsCurrYearInBin.reduce(
							(sumEuRatios, firm) => {
								sumEuRatios.cashFlow += Math.abs(euRatiosYearly[firm.firm_id.toString()].cashFlow);
								return sumEuRatios;
							},
							{cashFlow:0}
					);
					accu.euRatiosYearlysInBin.push(euRatiosCurrYearInBin);
					accu.numFirmsYearlysInBin.push(firmsCurrYearInBin.length);
					return accu;
				},
				{numFirmsYearlysInBin:[], euRatiosYearlysInBin:[]}
		);
		let gradNumFirmsYearlysInBin = [];
		numFirmsYearlysInBin.forEach((num, idx) => {
			let i = clamp(idx - 1, 0, numFirmsYearlysInBin.length - 1);
			gradNumFirmsYearlysInBin.push(num - numFirmsYearlysInBin[i]);
		});
		let gradEuRatiosYearlysInBin = [];
		euRatiosYearlysInBin.forEach((euRatios, idx) => {
			let i = clamp(idx - 1, 0, euRatiosYearlysInBin.length - 1);
			let cashFlowGrad = euRatios.cashFlow - euRatiosYearlysInBin[i].cashFlow;
			gradEuRatiosYearlysInBin.push({cashFlow: cashFlowGrad});
		});
		return ({
			centroid: viewport.unprojectFlat([hex.x, hex.y]),
			points: hex,
			numFirmsYearlys: numFirmsYearlysInBin,
			euRatiosYearlys: euRatiosYearlysInBin,
			gradNumFirmsYearlys: gradNumFirmsYearlysInBin,
			gradEuRatiosYearlys: gradEuRatiosYearlysInBin,
			index,
		});
	});

	return {
		hexagons: hexagons,
	};
}

class LocalHexagonLayer extends HexagonLayer {
	updateState({oldProps, props, changeFlags}) {
		const dimensionChanges = this.getDimensionChanges(oldProps, props);

		if (changeFlags.dataChanged !== false || this.needsReProjectPoints(oldProps, props)) {
			// project data into hexagons, and get sortedColorBins
			this.getHexagons();
		} else if (dimensionChanges) {
			dimensionChanges.forEach(f => typeof f === 'function' && f.apply(this));
		}
	}

	getSortedColorBins() {
		const {getColorValue} = this.props;
		const sortedColorBins = new BinSorter(this.state.hexagons || [], getColorValue);

		this.setState({sortedColorBins});
		this.getColorValueDomain();
	}

	getSortedElevationBins() {
		const {getElevationValue} = this.props;
		const sortedElevationBins = new BinSorter(this.state.hexagons || [], getElevationValue);
		this.setState({sortedElevationBins});
		this.getElevationValueDomain();
	}
	getDimensionUpdaters() {
		let superDimUpdaters = super.getDimensionUpdaters();
		let dimUpdaters = {...superDimUpdaters};
		dimUpdaters.getColor[2].triggers.push('timelineEnabled');
		dimUpdaters.getColor[2].triggers.push('timelineOption');

		return dimUpdaters;
	}

	getColorScale() {
		const {colorRange, timelineEnabled, timelineOption} = this.props;
		// const colorDomain = this.props.colorDomain || this.state.colorValueDomain;

		this.state.colorScaleFunc = ((colorDomain, value) => {
			let range = value < 0 ? NEGATIVE_COLORS : POSITIVE_COLORS;
			if (!timelineEnabled || timelineOption === 'firmDensity' || timelineOption == 'cashFlow') {
				range = colorRange;
			}
			return quantizeScale(colorDomain, range, Math.abs(value));
		});
	}


	_onGetSublayerColor(cell) {
		const {sortedColorBins, colorScaleFunc, colorValueDomain} = this.state;
		const cv = sortedColorBins.binMap[cell.index] && sortedColorBins.binMap[cell.index].value;
		const colorDomain = this.props.colorDomain || colorValueDomain;

		const isColorValueInDomain = Math.abs(cv) >= colorDomain[0] && Math.abs(cv) <= colorDomain[colorDomain.length - 1];

		// if cell value is outside domain, set alpha to 0
		const color = isColorValueInDomain ? colorScaleFunc(colorDomain, cv) : [0, 0, 0, 0];
		// add alpha to color if not defined in colorRange
		color[3] = Number.isFinite(color[3]) ? color[3] : 255;

		return color;
	}
}

function parseTimeline(currDate) {
	let year = Math.floor(currDate);
	let frac = currDate - year;
	let year_max = year;
	let s;
	if (frac > 0.25) {
		year_max = year_max + 1;
		s = frac - 0.25;
	} else if (frac < 0.25) {
		s = frac + 0.75;
	} else {
		s = 0;
	}
	return {
		year_max: year_max,
		s: s,
	}
}
export class CtLayers extends CompositeLayer {

	getPickingInfo (pickParams) {
		pickParams.info.layer = pickParams.sourceLayer;
		return pickParams.info;
	}

	getLocalHexagonElevationValue(hexagon, layerCfg) {
		if (!layerCfg.timelineEnabled) {
			return hexagon.points.length;
		}
		const {year_max, s} = parseTimeline(layerCfg.currDate);
		if (layerCfg.timelineOption === 'firmDensity') {
			if (year_max > 2007 && year_max <= 2016 && s !== 0) {
				let idx_next = year_max - 2007;
				let idx_prev = idx_next - 1;
				let count_prev = hexagon.numFirmsYearlys[idx_prev];
				let count_next = hexagon.numFirmsYearlys[idx_next];
				let count_curr = Math.round(lerp(count_prev, count_next, s));
				return count_curr;
			} else {
				let year = clamp(year_max, 2007, 2016);
				let idx = year - 2007;
				return hexagon.numFirmsYearlys[idx];
			}
		} else if (layerCfg.timelineOption === 'firmDensityGrad') {
			let year = clamp(year_max, 2008, 2016);
			let idx = year - 2007;
			return Math.abs(hexagon.gradNumFirmsYearlys[idx]);
		} else if (layerCfg.timelineOption === 'cashFlow') {
			if (year_max > 2007 && year_max <= 2016 && s !== 0) {
				let idx_next = year_max - 2007;
				let idx_prev = idx_next - 1;
				let cash_flow_prev = hexagon.euRatiosYearlys[idx_prev].cashFlow;
				let cash_flow_next = hexagon.euRatiosYearlys[idx_next].cashFlow;
				let cash_flow_curr = Math.round(lerp(cash_flow_prev, cash_flow_next, s));
				return Math.abs(cash_flow_curr);
			} else {
				let year = clamp(year_max, 2007, 2016);
				let idx = year - 2007;
				return Math.abs(hexagon.euRatiosYearlys[idx].cashFlow);
			}
		} else if (layerCfg.timelineOption === 'cashFlowGrad') {
			let year = clamp(year_max, 2007, 2014);
			let idx = year - 2007;
			return Math.abs(hexagon.gradEuRatiosYearlys[idx].cashFlow);
		} else {
			return 100;
		}

	}

	getLocalHexagonColorValue (hexagon, layerCfg) {
		if (!layerCfg.timelineEnabled) {
			return Math.log(hexagon.points.length + 1);
		}
		let {year_max, s} = parseTimeline(layerCfg.currDate);
		if (layerCfg.timelineOption === 'firmDensity') {
			if (year_max > 2007 && year_max <= 2016 && s !== 0) {
				let idx_next = year_max - 2007;
				let idx_prev = idx_next - 1;
				let count_prev = hexagon.numFirmsYearlys[idx_prev];
				let count_next = hexagon.numFirmsYearlys[idx_next];
				let count_curr = Math.round(lerp(count_prev, count_next, s));
				return Math.log(count_curr + 1);
			} else {
				let year = clamp(year_max, 2007, 2016);
				let idx = year - 2007;
				return Math.log(hexagon.numFirmsYearlys[idx] + 1);
			}
		} else if (layerCfg.timelineOption === 'firmDensityGrad') {
			let {year_max} = parseTimeline(layerCfg.currDate);
			let year = clamp(year_max, 2008, 2016);
			let idx = year - 2007;
			return hexagon.gradNumFirmsYearlys[idx];
		} else if (layerCfg.timelineOption === 'cashFlow') {
			if (year_max > 2007 && year_max <= 2016 && s !== 0) {
				let idx_next_cf = year_max - 2007;
				let idx_prev_cf = idx_next_cf - 1;
				let cash_flow_prev = hexagon.euRatiosYearlys[idx_prev_cf].cashFlow;
				let cash_flow_next = hexagon.euRatiosYearlys[idx_next_cf].cashFlow;
				let cash_flow_curr = Math.round(lerp(cash_flow_prev, cash_flow_next, s));
				return cash_flow_curr + 1;
			} else {
				let year_cf = clamp(year_max, 2007, 2016);
				let idx_cf = year_cf - 2007;
				return hexagon.euRatiosYearlys[idx_cf].cashFlow + 1;
			}
		} else if (layerCfg.timelineOption === 'cashFlowGrad') {
			let year = clamp(year_max, 2008, 2016);
			let idx = year - 2007;
			return hexagon.gradEuRatiosYearlys[idx].cashFlow;
		} else {
			return hexagon.points.length;
		}
	}

	getRegionalHexagonColorValue (data, layerCfg) {
		let minVal = layerCfg.regionElevationCriteria == "absolute" ? layerCfg.regionMinCount[layerCfg.regionElevationCriteria] : layerCfg.regionMinCount[layerCfg.regionElevationCriteria][layerCfg.regionLevel];
		let maxVal = layerCfg.regionElevationCriteria == "absolute" ? layerCfg.regionMaxCount[layerCfg.regionElevationCriteria] : layerCfg.regionMaxCount[layerCfg.regionElevationCriteria][layerCfg.regionLevel];
		const elevMin = Math.log(minVal);
		const elevMax = Math.log(maxVal);
		return colorMapping(HEATMAP_COLORS, Math.log(data.elevation), elevMin, elevMax);
	}

	getRegionalHexagonElevation(d, layerCfg) {
		const elevMin = layerCfg.regionElevationCriteria == "absolute" ? layerCfg.regionMinCount[layerCfg.regionElevationCriteria] : layerCfg.regionMinCount[layerCfg.regionElevationCriteria][layerCfg.regionLevel];
		const elevMax = layerCfg.regionElevationCriteria == "absolute" ? layerCfg.regionMaxCount[layerCfg.regionElevationCriteria] : layerCfg.regionMaxCount[layerCfg.regionElevationCriteria][layerCfg.regionLevel];

		//const elevMin = 1; //layerCfg.regionMinCount;
		//const elevMax = layerCfg.regionMinCount[layerCfg.regionElevationCriteria[layerCfg.regionLevel]];
		const elev = ((d.elevation - elevMin) * (regionalElevationRange[1] - regionalElevationRange[0])/(elevMax - elevMin) + regionalElevationRange[0]);
		return elev;
	}

	renderLayers() {
		const {localSelectedFirmsBySector, euRatioYearlySumMax, euRatiosYearlys, localFirms, regionalBins, firmsYearlys, smtCashFlows, onHover, onClick, layerCfg, currentTime} = this.props;
		if (localFirms.length === 0) return;
		const layers = [];
		if (layerCfg.view === 'localView') {
			if (layerCfg.hexagonVisible) {
				let elevation_domain = null;
				if (!layerCfg.timelineEnabled || layerCfg.timelineOption === 'firmDensity' || layerCfg.timelineOption === 'firmDensityGrad') {
					elevation_domain = [1, localFirms.length];
				} else if (layerCfg.timelineOption === 'cashFlow' || layerCfg.timelineOption === 'cashFlowGrad') {
					elevation_domain = [1, euRatioYearlySumMax.cashFlow];
				}
//            	layers.push(new Layer({
//            		id: 'blank-layer',
//            		pickable: true,
//            		onClick: info => onClick(info)
//            	}));
				layers.push(
						new ArcLauncher({
							id: 'local-cash-flow-arch',
							data: smtCashFlows.flowVecs,
							widthUnits: 'pixels',
							pickable: true,
							getStrokeWidth: 8, //d => Math.round(linearMapping(smtCashFlows.domain, LOCAL_ARC_WIDTH_RANGE, d.amount)),
							getSourcePosition: d => d.from.position,
							getTargetPosition: d => d.to.position,
							getSourceColor: d => d.sourceColor, //[178, 181, 183], // black
							getTargetColor: d => d.targetColor,
							//getTargetColor: [0,0,255,255], // blue
							//getFillColor: d => d.fillColor,
							onHover: info => {onHover(info)},
							currentTime: currentTime,
						}));
				layers.push(
						new LocalHexagonLayer({
							id: 'local-hexagon',
							hexagonAggregator: pointToHexbinFixedCenter,
							colorRange: HEATMAP_COLORS,
							elevationRange: localElevationRange,
							elevationDomain: elevation_domain,
							extruded: true,
							coverage: 0.98,
							getPosition: d => d.position,
							getCentroid: d => d.position,
							getColorValue: hexagon => this.getLocalHexagonColorValue(hexagon, layerCfg),
							getElevationValue: hexagon => this.getLocalHexagonElevationValue(hexagon, layerCfg),
							lightSettings: LIGHT_SETTINGS,
							opacity: 0.4,
							pickable: true,
							firmsYearlys: firmsYearlys,
							euRatiosYearlys: euRatiosYearlys,
							data: localSelectedFirmsBySector,
							onHover: info => onHover(info),
							onClick: info => onClick(info),
							radius:layerCfg.hexagonRadius,
							timelineEnabled: layerCfg.timelineEnabled,
							timelineOption: layerCfg.timelineOption,
						}));
			}
		} else if (layerCfg.view === 'regionalView') {
			layers.push(
					new HexagonCellLayer({
						id: 'regional-hexagon-cell',
						data: regionalBins[layerCfg.regionElevationCriteria],
						radius: layerCfg.regionRadius,
						coverage: 0.98,
						extruded: true,
						getCentroid: d => d.position,
						getElevation: d => this.getRegionalHexagonElevation(d, layerCfg),
						getColor: d => this.getRegionalHexagonColorValue(d, layerCfg),
						pickable: true,
						opacity: 0.4,
						onHover: info => onHover(info),
						onClick: info => onClick(info)
					})
			);
		}
		return layers;
	}
}


const TIMELINE_SLIDER_STYLES = {
		root: {
			position: "absolute",
			backgroundColor: "#f5f5f5",
			borderRadius: "5px",
			opacity: "0.85",
			top: "10px",
			height:"55px",
			left:"10%",
			width:"85%",
			zIndex: 7,
		},
		timelineSlider: {
			margin:"8px 10px 10px 10px",
			padding:"10px",
		},
}

const CT_CFG_STYLES = {
		root: {
			textAlign: 'left',
			display: 'block',
			opacity: "0.85",
			position: "absolute",
			top: "50px",
			zIndex: 8,
		},
		button: {
			color: '#607d8b',
			opacity: '0.75',
			zIndex: 9,
		},
		cfgPanel: {
			textAlign: 'left',
			display: 'flex',
			flexWrap: 'wrap',
			backgroundColor: "#f5f5f5",
			opacity: "0.95",
			width: 250,
			maxHeight: 650,
			padding: "5px 5px",
			marginTop: '5px',
			overflowY: 'auto',
			overflowX: 'hidden',
		},
		formControl: {
			padding: "5px 5px",
			width: 220,
		},

		timelineSwitch: {
			padding: "0px 5px",
			margin: "0px 0px",
		},
		timelineSwitchBase: {
			'&$timelineChecked': {
				color: "#2196f3",
				'& + $timelineBar': {
					backgroundColor: "#2196f3",
				},
			},
		},
		timelineBar: {},
		timelineChecked: {},

		hexagonRadiusSliderThumb: {
			backgroundColor: "#2196f3"
		},
		hexagonRadiusSlider: {
			padding: "15px 0px",
			width: 220,
		},
		hexagonRadiusSliderContainer: {
			padding: "5px 5px",
			width: 220,
		},
		label: {
			fontSize: "14px",
			color: "#607d8b",
			fontWeight: "bold"		
		},
		sectorInfo: {
			fontSize: "15px",
		},
		textContent: {
			fontSize: "15px",
			color: "#2196f3"
		},
		sectorPanel: {
			padding: "5px 5px",
			margin: "5px 0px",
			width: 220
		},

		sectorPanelSummary: {
			padding: "0px",
			margin: "0px 0px 10px 0px",
			width: 220
		},
		sectorPanelIcon: {
			padding: "0px 0px 0px 0px",
		},
		sectorDetailPanel: {
			padding: "0px 5px 5px 0px",
			margin: "0px 0px 0px 0px",
			flexGrow: 1,
		},
		sectorHeading: {
			fontSize: "13px",
			color: "#607d8b",
			flexBasis: '30%',
			flexShrink: 0,
		},
		sectorOptions: {
			fontSize: "13px",
			fontFamily: "Monospace",
		},
		sectorSecondHeading: {
			fontSize: "13px",
			fontFamily: "Monospace",
			color: "#2196f3",
			flexBasis: '55%',
			flexShrink: 0,
		},
		sectorButtonUp: {
			fontFamily: "Monospace",
			padding: "0px 0px 0px 0px",
			margin:"0px 0px 5px 0px",
			backgroundColor: "#b3e5fc",
			color: "#757575"
		},
		sectorButtonDown: {
			fontFamily: "Monospace",
			padding: "0px 0px 0px 0px",
			margin:"0px 0px 5px 0px",
			backgroundColor: "#4fc3f7",
			color: "#212121"
		},
		span: {
			fontSize: "16px",
			color: "#607d8b",
			fontWeight: "bold"
		},
};

const EXP_PANEL_STYLES = {
		heading_withmargin: {
			margin:"15px 0px 0px 0px",
		},
		root: {
			textAlign: 'left',
			display: 'block',
			opacity: "0.85",
			position: "absolute",
			bottom: "50px",
			zIndex: 8
		},
		text: {
			fontSize: "15px",
			color: "#2196f3",
			fontFamily: "Monospace",
		}
}

export function floatToDate(value) {
	let currDate = new Date('2000-01-01');
	let year = Math.floor(value);
	currDate.setFullYear(year);
	currDate.setDate(1 + Math.round(365 * (value - year)));
	return currDate;
}

class TimelineSliderComp extends React.Component {
	_onCurrDateChange = value => {
		const currLayerCfg = {
				...this.props.layerCfg,
				currDate: value
		};
		this.props.onLayerCfgChange(currLayerCfg);
	};

	render() {
		const { classes } = this.props;
		const { layerCfg } = this.props;
		let timelineSlider;
		if (layerCfg.timelineEnabled && layerCfg.view === 'localView' && layerCfg.hexagonVisible) {
			timelineSlider = (
					<div className={classes.root}>
					<div className={classes.timelineSlider}>
					<RcSlider
					min={2007}
					max={2014}
					defaultValue={layerCfg.currDate}
					included={true}
					marks={{
						2007:{style: {color: '#607d8b', fontSize:'13pt', fontWeight:"bold"}, label: 2007}, //add fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif" to match other fonts
						2008:{style: {color: '#607d8b', fontSize:'13pt', fontWeight:"bold"}, label:2008},
						2009:{style: {color: '#607d8b', fontSize:'13pt', fontWeight:"bold"}, label:2009},
						2010:{style: {color: '#607d8b', fontSize:'13pt', fontWeight:"bold"}, label:2010},
						2011:{style: {color: '#607d8b', fontSize:'13pt', fontWeight:"bold"}, label:2011},
						2012:{style: {color: '#607d8b', fontSize:'13pt', fontWeight:"bold"}, label:2012},
						2013:{style: {color: '#607d8b', fontSize:'13pt', fontWeight:"bold"}, label:2013},
						2014:{style: {color: '#607d8b', fontSize:'13pt', fontWeight:"bold"}, label:2014},
						//2015:{style: {color: '#607d8b', fontSize:'13pt', fontWeight:"bold"}, label:2015},
						//2016:{style: {color: '#607d8b', fontSize:'13pt', fontWeight:"bold"}, label:2016},
					}}
					step={0.025}
					trackStyle={{ backgroundColor: '#2196f3', height: 5 }}
					handleStyle={{
						borderColor: '#e0e0e0',
						height: 20,
						width: 20,
						marginLeft: -10,
						marginTop: -7,
						backgroundColor: '#2196f3',
					}}
					railStyle={{ backgroundColor: '#bbdefb', height: 4 }}
					onChange={this._onCurrDateChange}
					/>
					</div>
					</div>
			);
		} else {
			timelineSlider = (<div></div>);
		}
		return timelineSlider;
	}
}

class CtExpansionPanel extends React.Component{

	render() {

		const { layerCfg } = this.props;
		const { state } = this.props;
		const { classes } = this.props;
		if (layerCfg.view === 'localView')
			return (
					<div className={classes.root}>
					<ExpansionPanel>
					<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
					<Typography className={classes.heading}>Sector Information</Typography>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails>
					<div className={classes.text}>
					{state}
					<Typography className={classes.heading_withmargin}>Transaction Color Legend (€)</Typography>
					<ColorLegend {...layerCfg}
	                title={"testo"}
	                />						
					</div>				
					</ExpansionPanelDetails>
					</ExpansionPanel>
					</div> );
		else
			return (<div></div>);
	};

}

class CtLayerCfgComp extends React.Component {
	state = {
			panelIn: false,
			sectorPanelExpanded: false,
			modelOptions: ""
	};

	_onToggleSectorPanel = () => {
		this.setState(state => ({
			sectorPanelExpanded: !state.sectorPanelExpanded
		}));
	}

	_onTogglePanel = () => {
		this.setState(state => ({ panelIn: !state.panelIn }));
	};

	_onHexagonRadiusChange = (event, curr_value) => {
		const { layerCfg } = this.props;
		if (layerCfg["hexagonRadius"] !== curr_value) {
			const currLayerCfg = {
					...this.props.layerCfg,
					hexagonRadius: curr_value
			};
			this.props.onLayerCfgChange(currLayerCfg);
		}
	};

	_onToggleTimeline = event => {
		const currLayerCfg = {
				...this.props.layerCfg,
				timelineEnabled: event.target.checked
		};
		this.props.onLayerCfgChange(currLayerCfg);
	};

	_onToggleLocalHexagon = event => {
		const currLayerCfg = {
				...this.props.layerCfg,
				hexagonVisible: event.target.checked
		};
		this.props.onLayerCfgChange(currLayerCfg);
	};

	_onToggleLocalArc = event => {
		const currLayerCfg = {
				...this.props.layerCfg,
				localArcVisible: event.target.checked
		};
		this.props.onLayerCfgChange(currLayerCfg, true);
	};

	_onTimelineOptionChange = event => {
		const { layerCfg } = this.props;
		if (layerCfg["timelineOption"] !== event.target.value) {
			const currLayerCfg = {
					...this.props.layerCfg,
					timelineOption: event.target.value
			};
			this.props.onLayerCfgChange(currLayerCfg);
		}
	}

	_onModelSelectorChange = event => {
		const { layerCfg } = this.props;
		if (layerCfg["selectedModel"] !== event.target.value) {
			const currLayerCfg = {
					...this.props.layerCfg,
					selectedModel: event.target.value
			};
			this.props.onLayerCfgChange(currLayerCfg, true);
		}
	}

	_regionalNormalizationChange = event => {
		let newState = event.target.checked ? event.target.value : "absolute";
		const currLayerCfg = {
				...this.props.layerCfg,
				regionElevationCriteria: newState
		};
		this.props.onLayerCfgChange(currLayerCfg, true);
	};

	_onSectorButtonClick = (sector) => {
		const { layerCfg, oenaceLevelOne } = this.props;
		let selectedSectors = new Set(layerCfg.selectedSectors);
		if (sector === 'all') {
			if (selectedSectors.size === Object.keys(oenaceLevelOne).length) {
				selectedSectors.clear();
			} else {
				selectedSectors = new Set(Object.keys(oenaceLevelOne));
			}
		} else if (selectedSectors.has(sector)) {
			selectedSectors.delete(sector)
		} else {
			selectedSectors.add(sector)
		}
		const currLayerCfg = {
				...this.props.layerCfg,
				selectedSectors: selectedSectors
		};
		this.props.onSelectedSectorsChange(currLayerCfg);
	}

	render() {
		const {panelIn, sectorPanelExpanded} = this.state;
		const { classes } = this.props;
		const { layerCfg, mapStyle, oenaceLevelOne, defaultModel } = this.props;
		const hexRadius = layerCfg.hexagonRadius;
		const settingsButton = panelIn ? <SettingsOutline/> : <Settings/>;
		let hexagonSlider;
		let regionLevelSelector;
		let sectorPanel;
		let timelineSwitch;
		let timelineOptionSelector;
		let modelSelector;
		let currDate;
		//let detailsPanel;
		let localHexagonSwitch, localArcSwitch;
		let selectedSectors = Object.keys(oenaceLevelOne).map(
				(curr, idx) => {
					let label = layerCfg.selectedSectors.has(curr) ? <span>{curr}</span> : <span>&nbsp;</span>;
					let sectorText = idx % 7 === 6 ? label : <span>{label} </span>;
					return idx % 7 === 6 ? <span key={idx}>{sectorText}<br /></span> : <span key={idx}>{sectorText}</span>;
				}
		);
		let sectorButtons = Object.keys(oenaceLevelOne).map(
				curr => (
						<Grid item xs={4}  key={curr}>
						<Tooltip title={oenaceLevelOne[curr]} classes={{ tooltip: classes.sectorInfo }}>
						<Button
						className={layerCfg.selectedSectors.has(curr)? classes.sectorButtonDown: classes.sectorButtonUp}
						size="small"
							onClick={() => this._onSectorButtonClick(curr)}
						>
						{curr}
						</Button>
						</Tooltip>
						</Grid>
				));

		if (layerCfg.view === 'localView') {
			localArcSwitch = (
					<FormControlLabel
					className={classes.timelineSwitch}
					control={
							<Switch
							checked={layerCfg.localArcVisible}
							onChange={this._onToggleLocalArc}
							value="localArcVisible"
								classes={{
									switchBase: classes.timelineSwitchBase,
									checked: classes.timelineChecked,
									bar: classes.timelineBar,
								}}
							/>
					}
					label={<Typography htmlFor="localArcVisible" className={classes.label}>Arc</Typography>}
					labelPlacement="start"
						/>
			);

			localHexagonSwitch = (
					<FormControlLabel
					className={classes.timelineSwitch}
					control={
							<Switch
							checked={layerCfg.hexagonVisible}
							onChange={this._onToggleLocalHexagon}
							value="hexagonVisible"
								classes={{
									switchBase: classes.timelineSwitchBase,
									checked: classes.timelineChecked,
									bar: classes.timelineBar,
								}}
							/>
					}
					label={<Typography htmlFor="hexagonVisible" className={classes.label}>Hexagon</Typography>}
					labelPlacement="start"
						/>
			);

			if (layerCfg.hexagonVisible) {
				timelineSwitch = (
						<FormControlLabel
						className={classes.timelineSwitch}
						control={
								<Switch
								checked={layerCfg.timelineEnabled}
								onChange={this._onToggleTimeline}
								value="timelineEnabled"
									classes={{
										switchBase: classes.timelineSwitchBase,
										checked: classes.timelineChecked,
										bar: classes.timelineBar,
									}}
								/>
						}
						label={<Typography htmlFor="Timeline" className={classes.label}>Timeline</Typography>}
						labelPlacement="start"
							/>
				);
				if (layerCfg.timelineEnabled) {
					currDate = (
							<div className={classes.hexagonRadiusSliderContainer}>
							<Typography htmlFor="currDate" className={classes.label}>
							Current date: <span className={classes.textContent}>
							{floatToDate(layerCfg.currDate).toISOString().substring(0, 10)}
							</span>
							</Typography>
							</div>
					);

					timelineOptionSelector = (
							<FormControl className={classes.formControl}>
							<Typography htmlFor="timeline-option" className={classes.label}>Timeline option</Typography>
							<Select
							value={layerCfg.timelineOption}
							onChange={this._onTimelineOptionChange}
							inputProps={{
								name: 'timeline-option',
								id: 'timeline-option',
							}}
							>
							<MenuItem value='firmDensity' key='firmDensity'>
							<Typography className={classes.textContent}>firm density</Typography>
							</MenuItem>
							<MenuItem value='firmDensityGrad' key='firmDensityGrad'>
							<Typography className={classes.textContent}>firm density grad</Typography>
							</MenuItem>
							<MenuItem value='cashFlow' key='cashFlow'>
							<Typography className={classes.textContent}>cash flow</Typography>
							</MenuItem>
							<MenuItem value='cashFlowGrad' key='cashFlowGrad'>
							<Typography className={classes.textContent}>cash flow grad</Typography>
							</MenuItem>
							</Select>
							</FormControl>
					);
					let menuItems = [];
					var models = layerCfg.models;
					if(models !== undefined){
						menuItems = models.map((obj) => {
							return (
									<MenuItem value={obj.id} key={obj.id}>
									<Typography className={classes.textContent}>{obj.label}</Typography>
									</MenuItem>
							);
						});
					}
					var selectedModel = layerCfg.selectedModel == undefined ? "" : layerCfg.selectedModel;
					modelSelector = (
							<FormControl className={classes.formControl}>
							<Typography htmlFor="model-option" className={classes.label}>Available models</Typography>
							<Select
							value={selectedModel}
							onChange={this._onModelSelectorChange}
							inputProps={{
								name: 'model-option',
								id: 'model-option',
							}}
							>
							{menuItems}
							</Select>
							</FormControl>
					);
				}
				hexagonSlider = (
						<div className={classes.hexagonRadiusSliderContainer}>
						<Typography id="hexagon-radius-icon" className={classes.label}>
						Hexagon radius: <span className={classes.textContent}>{hexRadius}</span> meters
						</Typography>
						<Slider
						value={hexRadius}
						min={25}
						max={6000}
						step={25}
						aria-labelledby="hexagon-radius-icon"
							onChange={this._onHexagonRadiusChange}
						classes={{
							container: classes.hexagonRadiusSlider,
							thumb: classes.hexagonRadiusSliderThumb,
							track: classes.hexagonRadiusSliderThumb,
						}}
						/>
						</div>
				);

				let isButtonDown = layerCfg.selectedSectors.size === Object.keys(oenaceLevelOne).length;
				sectorPanel = (
						<ExpansionPanel className={classes.sectorPanel} expanded={sectorPanelExpanded} onChange={this._onToggleSectorPanel}>
						<ExpansionPanelSummary
						className={classes.sectorPanelSummary}
						classes={{
							expandIcon: classes.sectorPanelIcon,
						}}
						expandIcon={<ExpandMoreIcon />}>
						<Typography className={classes.sectorHeading}>Selected sectors: </Typography>
						<Typography className={classes.sectorSecondHeading}>{selectedSectors}</Typography>
						</ExpansionPanelSummary>
						<ExpansionPanelDetails className={classes.sectorDetailPanel}>
						<Grid container>
						<Grid container spacing={0} justify="center" alignItems='flex-start' >
						<Grid item xs={4}>
						<Button
						className={isButtonDown? classes.sectorButtonDown: classes.sectorButtonUp}
						size="large"
							onClick={() => this._onSectorButtonClick('all')}
						>
						all
						</Button>
						</Grid>
						</Grid>
						<Grid container spacing={0}>
						{sectorButtons}
						</Grid>
						</Grid>
						</ExpansionPanelDetails>
						</ExpansionPanel>
				);

			}
		} else if (layerCfg.view === 'regionalView') {
			regionLevelSelector = (
					<FormControl className={classes.formControl}>
					<Typography htmlFor="region-level-option" className={classes.label}>Region level</Typography>
					<Select
					value={layerCfg.regionLevel}
					onChange={this.props.onRegionLevelChange}
					inputProps={{
						name: 'region-level-option',
						id: 'region-level-option',}}
					>
					<MenuItem value='bundesland' key='Bundesland'>
					<Typography className={classes.textContent}>Bundesland</Typography>
					</MenuItem>
					<MenuItem value='bezirk' key='Bezirk'>
					<Typography className={classes.textContent}>Bezirk</Typography>
					</MenuItem>
					<MenuItem value='gemeinde' key='Gemeinde'>
					<Typography className={classes.textContent}>Gemeinde</Typography>
					</MenuItem>
					<MenuItem value='ort' key='ort'>
					<Typography className={classes.textContent}>Ort</Typography>
					</MenuItem>
					</Select>
					<FormControlLabel
					className={classes.timelineSwitch}
					control={
							<Switch
							checked={layerCfg.regionElevationCriteria == "normalized"}
							onChange={this._regionalNormalizationChange}
							value="normalized"
								classes={{
									switchBase: classes.timelineSwitchBase,
									checked: classes.timelineChecked,
									bar: classes.timelineBar,
								}}
							/>
					}
					label={<Typography htmlFor="Timeline" className={classes.label}>Normalize Values</Typography>}
					labelPlacement="start"
						/>
					</FormControl>);
		}
		return (
				<div className={classes.root}>
				<Fab size='small' className={classes.button} onClick={this._onTogglePanel} aria-label="Collapse">
				{settingsButton}
				</Fab>
				<Collapse in={panelIn}>
				<Paper className={classes.cfgPanel}>
				<FormControl className={classes.formControl}>
				<Typography htmlFor="mapbox-map-style" className={classes.label}>Map style</Typography>
				<Select
				value={mapStyle}
				onChange={this.props.onMapStyleChange}
				inputProps={{
					name: 'mapbox-map-style',
					id: 'mapbox-map-style',}}
				>
				{MAPBOX_DEFAULT_MAPSTYLES.map(style => (
						<MenuItem value={style.value} key={style.value}>
						<Typography className={classes.textContent}>{style.label}</Typography>
						</MenuItem>
				))}
				</Select>
				</FormControl>
				<FormControl className={classes.formControl}>
				<Typography htmlFor="view-option" className={classes.label}>View option</Typography>
				<Select
				value={layerCfg.view}
				onChange={this.props.onViewOptionChange}
				inputProps={{
					name: 'view-option',
					id: 'view-option',}}
				>
				<MenuItem value='localView' key='localView'>
				<Typography className={classes.textContent}>Local</Typography>
				</MenuItem>
				<MenuItem value='regionalView' key='regionalView'>
				<Typography className={classes.textContent}>Regional</Typography>
				</MenuItem>
				</Select>
				</FormControl>
				{localHexagonSwitch}
				{localArcSwitch}
				{modelSelector}
				{timelineSwitch}
				{timelineOptionSelector}
				{currDate}
				{hexagonSlider}
				{regionLevelSelector}
				{sectorPanel}
				</Paper>
				</Collapse>
				</div>
		);
	}
}

const CtLayerCfg = withStyles(CT_CFG_STYLES)(CtLayerCfgComp);
const TimelineSlider = withStyles(TIMELINE_SLIDER_STYLES)(TimelineSliderComp);
const DetailsPanel = withStyles(EXP_PANEL_STYLES)(CtExpansionPanel);
export {CtLayerCfg, TimelineSlider, DetailsPanel};
