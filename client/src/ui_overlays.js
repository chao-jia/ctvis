import React from "react";
import { charts } from "./style";
import { XAxis, XYPlot, YAxis, VerticalRectSeries } from "react-vis";
// import {range, color_scales} from "./app";
import Chroma from "chroma-js";

// const arc_scale = {};


function calculateDistrBins(min,max,num_bins) {

    let range = {};
    //calculate val min/max & range
    range.min = min;
    range.max = max;
    range.num_bins = num_bins;

    range.range = range.max - range.min;
    range.interval = range.range / num_bins;
    range.bins = [];

    for(let i=0; i<num_bins; i++)
    {
        let binobj = {};
        binobj.val = i*range.interval + range.min;
        binobj.distr = 0;
        range.bins.push(binobj);
    }
    let ticks = range.bins.map(d => d.val);
    ticks.push(range.max);
    range.ticks = ticks;

    return range;
}


export default function ColorLegend({ colorLegend }) {

    let range = {};

    if((Object.entries(colorLegend).length === 0 && colorLegend.constructor === Object) || colorLegend === undefined)
        return(<div></div>);

    range = calculateDistrBins(colorLegend.min, colorLegend.max, colorLegend.num_bins);
    var greenScale = Chroma.scale(['#47ff4d','#006803']).domain([range.min, range.max]);
    var redScale = Chroma.scale(['#ffb5b5','#f73600']).domain([range.min, range.max]);

    const col_bins = range.bins.map(d => ({x0: d.val, x: d.val + range.interval, y: 0, y: 20}));

    if (!col_bins) {
        return (<div style={charts} />);
    }
    const ch_data = col_bins.map(d => {
        let color = greenScale(d.x0 ).hex();
        return { ...d, color };
    });
    const ch_data_r = col_bins.map(d => {
        let color = redScale(d.x0 ).hex();
        d.y0 = d.y;
        d.y = d.y*2;
        return { ...d, color };
    });

    return (<div style={charts}>
    <XYPlot
    getX={d => d.x}
    getY={d => d.y}
    margin={{ left: 10, right: 25, top: 0, bottom: 25 }}
    height={60}
    width={300}
    yDomain={[0, 100]}
>

    <VerticalRectSeries
    colorType="literal"
    data={ch_data}
    style={{ cursor: 'pointer' }}
    />
    <VerticalRectSeries
        colorType="literal"
        data={ch_data_r}
        style={{ cursor: 'pointer' }}
    />
    <XAxis
    tickFormat={d => parseInt(d/1000)+"K"}
    tickValues={range.ticks}
        />
    </XYPlot>
    </div>);
}