import React, { Component } from 'react';
import ReactMapGL, {NavigationControl} from 'react-map-gl';
import { CtLayers, CtLayerCfg, TimelineSlider, DetailsPanel} from './ct-layers';
import DeckGL, {MapController} from 'deck.gl';
// import * as d3 from 'd3';
import chroma from 'chroma-js';
//import d3-scale-chromatic as Chroma from 'd3-scale-chromatic';
import 'mapbox-gl/dist/mapbox-gl.css';
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
//import ColorLegend from './ui_overlays';

//import smtModel from './25-370M-1sec.json';

const TOKEN = 'pk.eyJ1IjoidWJlcmRhdGEiLCJhIjoiY2pudzRtaWloMDAzcTN2bzN1aXdxZHB5bSJ9.2bkj3IiRC8wj3jLThvDGdA';

const MAP_VIS_STYLE = {
    hoverLabel: {
        position: 'absolute',
        padding: '4px',
        background: 'rgba(0, 0, 0, 0.7)',
        color: '#fff',
        maxWidth: '300px',
        zIndex: 9,
        pointerEvents: 'none',
        color: 'white'
    },
    labelText: {
        fontSize: '13px',
        color: 'white',
        whiteSpace: 'pre-wrap'
    },
    loader: {
        position: 'absolute',
        left: '50%',
        top: '50%',
        zIndex: 10,
        width: '150px',
        height: '150px',
        margin: '-75px 0 0 -75px',
        border: '16px solid #f3f3f3',
        borderRadius: '50%',
        borderTop: '16px solid #3498db',
        animation: 'spinAnimation 2s linear infinite',
    },
    '@keyframes spinAnimation': {
        '0%': {transform: 'rotate(0deg)'},
        '100%': {transform: 'rotate(360deg)'},
    }

};

const ARC_COLORS = [
    [255, 255, 178],
    [254, 217, 118],
    [254, 178, 76],
    [253, 141, 60],
    [252, 78, 42],
    [227, 26, 28],
    [177, 0, 38]
  ];

const MAPVIS_INIT = {
    view: 'localView',
};

const ARC_LAUNCHER_INTERVAL = 50; // milliseconds
const ARC_FLOW_VELOCITY = 3; // from 0 to 100
class MapVis extends Component {
    constructor(props) {
        super(props);
        const MIN_ZOOM = 6, MAX_ZOOM = 18, ZOOM = 7;
        let radius = this._calcHexagonRadius(ZOOM, MIN_ZOOM, MAX_ZOOM);
        this.state = {
            currentTime: 0,
            hover: {
                x: 0,
                y: 0,
                hoveredObject: null
            },
            click: {
            	x: 0,
            	y: 0,
            	clickedObject: null
            },
            regionalBins:{},
            localFirms: [],
            localFirmIndicesObj: {},
            localSelectedFirmsBySector: [],
            firmsYearlys:[],
            euRatiosYearlys:[],
            euRatioYearlySumMax: {
                cashFlow: 1,
            },
            smtCashFlows:{},
            layerCfg: {
                view: MAPVIS_INIT.view,
                hexagonRadius: radius,
                hexagonVisible: true,
                localArcVisible: true,
                timelineOption: 'firmDensity',
                regionLevel: 'bundesland',
                regionElevationCriteria: 'absolute',
                regionMinCount: 1,
                regionMaxCount: 1,
                regionRadius: 500,
                selectedSectors: new Set(),
                timelineEnabled: true,
                currDate: 2007,
                colorLegend : {}
            },
            viewState: {
                longitude: 13.2420508,
                latitude: 47.567685,
                zoom: 7,
                minZoom: MIN_ZOOM,
                maxZoom: MAX_ZOOM,
                pitch: 60,
                bearing: 0
            },
            oenaceLevelOne: {},
            initFinished: false,
            mapStyle: 'mapbox://styles/mapbox/light-v9'

        };
        this._increase();
    }
    _increase() {
    	if(this.state.currentTime + ARC_FLOW_VELOCITY < 100){
	        const t = (this.state.currentTime + ARC_FLOW_VELOCITY) % 100;
	        this.setState({ currentTime: t});
	        setTimeout(this._increase.bind(this), ARC_LAUNCHER_INTERVAL);
    	}
    }

    async componentDidMount() {
        console.log("loading data...");
        let available_models = await this._retrieveAvailableModels();
    	this.state.layerCfg.selectedModel = available_models[0].id;
    	this.state.layerCfg.models = available_models;
        if (MAPVIS_INIT.view === 'localView') {
            let localState = await this._loadLocalViewData(this.state.layerCfg);
            this.setState({...localState, initFinished: true});
        } else if (MAPVIS_INIT.view === 'regionalView') {
            let regionState = await this._loadRegionalViewData(this.state.layerCfg);
            this.setState({...regionState, initFinished: true});
        }
    }

    _parseSmtModel = (smtModel, localFirms, localFirmIndicesObj, idsInHexagon) => { //I need to change this method so it accepts the db output
        //let firm_smt_indices = Object.keys(smtModel);
        let firm_smt_positions = {};
        smtModel.edges.forEach( value => {
        	let firm_ids = [];
        	firm_ids[0] = value.source;
        	firm_ids[1] = value.target;
            let index_in_local_firms;
            for(var i = 0; i<2; i++){
            	index_in_local_firms = localFirmIndicesObj[firm_ids[i].toString()];
	            if (index_in_local_firms === undefined) {
	                console.log("cannot find firm with id = " + firm_ids[i]);
	                let firm_pos = [12.335833, 45.4375]; // Venice
	                firm_smt_positions[firm_ids[i]] = firm_pos;
	            } else {
	                let firm_pos = localFirms[index_in_local_firms].position;
	                firm_smt_positions[firm_ids[i]] = firm_pos;
	            }
            }
        });

        let smtCashFlows = [];
//        let minFlowAmount = Number.MAX_VALUE;
//        let maxFlowAmount = 0;
        let outgoingFlow = 0;
        let ingoingFlow = 0;
        let internalMovement = 0;
        smtModel.edges.forEach( value => {
        		if(idsInHexagon.includes(value.source) && idsInHexagon.includes(value.target))
        			internalMovement += value.weight;
        		else
        			if(idsInHexagon.includes(value.source))
        				outgoingFlow += value.weight;
        			else
        				ingoingFlow += value.weight;
//            smtModel[model_id].outflows.forEach((flow, dest_id) => {
                const smt_cash_flow = {
                    from: {
                        firmId: value.source,
                        position: [...firm_smt_positions[value.source]],
                    },
                    to: {
                    	firmId: value.target,
                        position: [...firm_smt_positions[value.target]],
                    },
                    amount: value.weight,
                };
                smtCashFlows.push(smt_cash_flow);
                /*minFlowAmount = minFlowAmount < flow ? minFlowAmount : flow;
                maxFlowAmount = maxFlowAmount > flow ? maxFlowAmount : flow;*/
//            });
        });

        let colorLegend = {}
        colorLegend.min =smtModel.intervals[0].min;
        colorLegend.max = smtModel.intervals[0].max;
        colorLegend.num_bins = 5;
        this.state.layerCfg.colorLegend = colorLegend;
        /*this.setState(this.state);
        console.log("state set:")
        console.log(this.state)*/
        
        //this._updateLayerCfg(this.state.layerCfg);

        return {
            flowVecs: smtCashFlows,
            domain: [smtModel.intervals[0].min, smtModel.intervals[0].max],
        	flows: [ingoingFlow, internalMovement, outgoingFlow],
        };
    };

    _loadLocalViewData = async (layerCfg) => {
        console.log("loading local data...");
        const localFirms = [];
        const localFirmIndicesObj = {};

        const parseLocalData = ((row, idx) => {
            let lat = Number(row.latitude);
            let lng = Number(row.longitude);
            let firm_id = Number(row.firm_id);
            let edv_e1 = row.edv_e1;
            localFirms.push({position:[lng, lat], firm_id:firm_id, edv_e1: edv_e1});
            localFirmIndicesObj[firm_id.toString()] = idx;
        });

        try {
            const localData = await this._retrieveLocalData(layerCfg);
            localData.forEach((row, idx) => {parseLocalData(row, idx);});
            layerCfg.regionMaxCount = 1;
            layerCfg.regionMinCount = 1;
            layerCfg.selectedSectors.clear();
            let oenaceLevelOne = {};
            let euRatiosYearlys = [];
            let firmsYearlys = [];

            const oenace = await this._retriveOenaceData();
            oenace.forEach(sec => {
                layerCfg.selectedSectors.add(sec.edv_e1);
                oenaceLevelOne[sec.edv_e1] = sec.desc;
            });

            let eu_ratios_data = await this._retrieveEuRatiosData();
            let eu_ratios = eu_ratios_data.ratios;
            let eu_ratios_intervals = eu_ratios_data.intervals;

            let eu_ratio_yearly_sum_max = {cashFlow: 1};
            for (let i = 0; i < eu_ratios_intervals.length - 1; ++i) {
                let beg = eu_ratios_intervals[i];
                let end = eu_ratios_intervals[i + 1];
                let firm_id_set = new Set();
                let eu_ratio_obj = {};
                let cash_flow_yearly_sum = 0;
                eu_ratios.slice(beg, end).forEach( firm => {
                    firm_id_set.add(firm.firm_id);
                    eu_ratio_obj[firm.firm_id.toString()] = {cashFlow: firm.cash_flow};
                    cash_flow_yearly_sum += Math.abs(firm.cash_flow);
                });
                eu_ratio_yearly_sum_max.cashFlow = Math.max(cash_flow_yearly_sum, eu_ratio_yearly_sum_max.cashFlow);
                firmsYearlys.push(firm_id_set);
                euRatiosYearlys.push(eu_ratio_obj);
            }
            //let smtCashFlows = this._parseSmtModel(smtModel, localFirms, localFirmIndicesObj);
            // console.log("smtCashFlows: \n" + JSON.stringify(smtCashFlows));

            let smtCashFlows = {};

            let localSelectedFirmsBySector = [...localFirms];
            return {
                localFirms: localFirms,
                localFirmIndicesObj: localFirmIndicesObj,
                localSelectedFirmsBySector: localSelectedFirmsBySector,
                firmsYearlys: firmsYearlys,
                euRatiosYearlys: euRatiosYearlys,
                smtCashFlows: smtCashFlows,
                layerCfg: layerCfg,
                oenaceLevelOne: oenaceLevelOne,
                euRatioYearlySumMax: eu_ratio_yearly_sum_max,
            };

        } catch(err) {
            console.log(err);
        };
    };

    _loadRegionalViewData = async (layerCfg) => {
        console.log("loading regional data...");
        const regionalBins = {};
        regionalBins["normalized"] = [];
        regionalBins["absolute"] = [];
        let maxCounts = {};
        let minCounts = {};
        const parseRegionalData = (row => {
            let lat = Number(row.latitude);
            let lng = Number(row.longitude);
            let firmCount = Number(row.count);
            let bundesland = <div>{row.bundesland}</div>;
            if(maxCounts[layerCfg.regionLevel] == null)
              maxCounts[layerCfg.regionLevel] = Number.MIN_SAFE_INTEGER;
            if(minCounts[layerCfg.regionLevel] == null)
              minCounts[layerCfg.regionLevel] = Number.MAX_SAFE_INTEGER;
            if (layerCfg.regionLevel === 'bundesland') {
                layerCfg.regionRadius = 12000;
                let normValue = Math.round(firmCount/layerCfg.regionRadius*10000)/100;
                maxCounts[layerCfg.regionLevel] = Math.max(maxCounts[layerCfg.regionLevel], normValue);
                minCounts[layerCfg.regionLevel] = Math.min(minCounts[layerCfg.regionLevel], normValue);

                regionalBins["normalized"].push({position:[lng, lat], elevation: normValue, label: bundesland});
                regionalBins["absolute"].push({position:[lng, lat], elevation: firmCount, label: bundesland});
            } else {
                let bezirk = <div><div>{row.bezirk}</div>{bundesland}</div>;
                if (layerCfg.regionLevel === 'bezirk') {
                    layerCfg.regionRadius = 2500;
                    let normValue = Math.round(firmCount/layerCfg.regionRadius*10000)/100;
                    maxCounts[layerCfg.regionLevel] = Math.max(maxCounts[layerCfg.regionLevel], normValue);
                    minCounts[layerCfg.regionLevel] = Math.min(minCounts[layerCfg.regionLevel], normValue);

                    regionalBins["normalized"].push({position:[lng, lat], elevation:  Math.round(firmCount/layerCfg.regionRadius*10000)/100, label: bundesland});
                    regionalBins["absolute"].push({position:[lng, lat], elevation: firmCount, label: bundesland});                } else {
                    let gemeinde = <div><div>{row.gemeinde}</div>{bezirk}</div>;
                    if (layerCfg.regionLevel === 'gemeinde') {
                        layerCfg.regionRadius = 1000;
                        let normValue = Math.round(firmCount/layerCfg.regionRadius*10000)/100;
                        maxCounts[layerCfg.regionLevel] = Math.max(maxCounts[layerCfg.regionLevel], normValue);
                        minCounts[layerCfg.regionLevel] = Math.min(minCounts[layerCfg.regionLevel], normValue);

                        regionalBins["normalized"].push({position:[lng, lat], elevation:  Math.round(firmCount/layerCfg.regionRadius*10000)/100, label: bundesland});
                        regionalBins["absolute"].push({position:[lng, lat], elevation: firmCount, label: bundesland});                    } else if (layerCfg.regionLevel === 'ort') {
                        layerCfg.regionRadius = 300;
                        let ort = <div><div>{row.ort}</div>{gemeinde}</div>;
                        regionalBins["normalized"].push({position:[lng, lat], elevation:  Math.round(firmCount/layerCfg.regionRadius*10000)/100, label: bundesland});
                        regionalBins["absolute"].push({position:[lng, lat], elevation: firmCount, label: bundesland});                    }
                }
            }
        });

        try {
            const body = await this._retrieveRegionalData(layerCfg);
            body.forEach(row => {parseRegionalData(row);});
            // ensured by sql query: order by count desc
            let regionMaxes = {};
            let regionMins = {};

            layerCfg.regionMaxCount = {};
            regionMaxes["absolute"] = body[0].count;
            regionMins["absolute"] = Math.ceil(body[body.length - 1].count / 1.25);
            regionMaxes["normalized"] = maxCounts;
            regionMins["normalized"] = minCounts;

            layerCfg.regionMaxCount = regionMaxes;
            layerCfg.regionMinCount = regionMins;

            let state = {
                regionalBins: regionalBins,
                layerCfg: layerCfg,
            };
            return state;
        } catch(err) {
            console.log(err);
            return {};
        };
    };


    _retriveOenaceData = async () => {
        try {
            const response = await fetch('/oenace_data',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({})
            });
            const body = await response.json();
            if (response.status !== 200) throw Error(body.message);
            return body;
        } catch (err) {
            console.log(err.message, err.stack);
            return [];
        }
    }

    _retrieveRegionalData = async (layerCfg) =>  {
        try {
            const response = await fetch('/regional_data',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({regionLevel: layerCfg.regionLevel })
            });
            const regionalData = await response.json();
            if (response.status !== 200) throw Error(regionalData.message);
            return regionalData;
        } catch (err) {
            console.log(err.message, err.stack);
            return [];
        }
    };

    _retrieveLocalData = async (layerCfg) =>  {
        try {
            const response = await fetch('/local_data',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({view: layerCfg.view, model_options: {filtered: layerCfg.localArcVisible}, model: layerCfg.selectedModel})
            });
            const localData = await response.json();
            if (response.status !== 200) throw Error(localData.message);
            return localData;
        } catch (err) {
            console.log(err.message, err.stack);
            return [];
        }
    };

    _retrieveAvailableModels = async () =>  {
        try {
            const response = await fetch('/available_models',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                }//,
                //body: JSON.stringify({view: layerCfg.view})
            });
            const localData = await response.json();
            if (response.status !== 200) throw Error(localData.message);
            return localData;
        } catch (err) {
            console.log(err.message, err.stack);
            return [];
        }
    };

    _retrieveEuRatiosData = async () =>  {
        try {
            const response = await fetch('/eu_ratios_data',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({})
            });
            const eu_ratios_data = await response.json();
            if (response.status !== 200) throw Error(eu_ratios_data.message);
            return eu_ratios_data;
        } catch (err) {
            console.log(err.message, err.stack);
            return {};
        }
    };

    _retrieveModelEdges = async (layerCfg, year, sources) => { //remove dummy queries
    	//year = 2012;
    	//sources = [33524];
        try {
//        	console.log(JSON.stringify({year: year, sources: sources }));
            const response = await fetch('/model_query',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({model_id: layerCfg.selectedModel, year: year, sources: sources })
            });
            const body = await response.json();
            if (response.status !== 200) throw Error(body.message);
//            console.log(body);
            return body;
        } catch (err) {
            console.log(err.message, err.stack);
            return [];
        }
     };

    // not in use
    _retrieveGeoSecData = async (layerCfg) =>  {
        try {
            const response = await fetch('/geo_sec_data',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({view: layerCfg.view, regionLevel: layerCfg.regionLevel })
            });
            const body = await response.json();
            if (response.status !== 200) throw Error(body.message);
            return body;
        } catch (err) {
            console.log(err.message, err.stack);
            return [];
        }
    };

    _onHover = ({ x, y, object, layer}) => {
    	const { classes } = this.props;
        let label = null;
        let suffix = null;
        let layerCfg = this.state.layerCfg;
        if (object) {
            if (layerCfg.view === 'localView' ) {
                if (layer.id === 'local-hexagon') {
                    let buckets = {};
                    object.points.map(function(element){
                      if(buckets[element.edv_e1] === undefined || buckets[element.edv_e1] === null)
                        buckets[element.edv_e1] = 1;
                      else {
                        buckets[element.edv_e1] += 1;
                      }
                    });
                    let sector_text = "";
                    this._sortObjectProps(buckets, function(key, value){
                        sector_text += "Sector " + key + " " + Math.round(value/object.points.length*10000)/100 + "%\n";
                      });
                    if (!layerCfg.timelineEnabled || layerCfg.timelineOption === 'firmDensity' || layerCfg.timelineOption === 'firmDensityGrad') {
                        let label_text = object.points.length /*object.elevationValue*/ * Math.sign(object.colorValue) + (object.points.length /*object.elevationValue*/ > 1 ? ' firms' : ' firm');
                        label_text += "\n" + sector_text;
                        label = <Typography className={classes.labelText}>{label_text}</Typography>;
                    }  else if (layerCfg.timelineOption === 'cashFlow' || layerCfg.timelineOption === 'cashFlowGrad') {
                        let value = object.points.length; //object.elevationValue;
                        suffix = this._getSuffixString(value);
                        suffix[0] *= Math.sign(object.colorValue);
                        let label_text = suffix[0]+suffix[1];
                        label_text += "\n" + sector_text;
                        label = (<Typography className={classes.labelText}>{label_text}</Typography>);
                    } else {
                        label = null;
                    }
                } else if (layer.id === 'local-cash-flow-arch') {
                	suffix = this._getSuffixString(object.amount);
                    label = <Typography className={classes.labelText}>from {object.from.firmId} to {object.to.firmId}: {suffix[0]}{suffix[1]}</Typography>;
                } else {
                    label = null;
                }
            } else if (layerCfg.view === 'regionalView') {
                let firm_text = (object.elevation > 1 ? ' firms' : ' firm') ;
                if(layerCfg.regionElevationCriteria == "normalized")
                  firm_text ='firms/km²';
                label = <Typography className={classes.labelText}>{object.label}{object.elevation} {firm_text}</Typography>;
            }
        }
        else {
            label = null;
        }
        this.setState({ hover: { x, y, hoveredObject: object, label } });
    };

    _onClick = ({ x, y, object, layer}) => {
//    	console.log("Click detected");
    	//if (layerCfg.view === 'localView' ) {
            if (layer.id === 'local-hexagon') {
//            	console.log(object);
            	this.setState({click: {x, y}});
            	this._updateSMT(object);
            }else{
            	this.state.smtCashFlows = {};
            	this._clearClickAndHover();
            }

    	//}
    };

    _updateSMT = async (object = null) => {
        let layerCfg = this.state.layerCfg;
    	var ids = [];
        let label = "";
    	if(object){
	    	for(var firm_proto in object.points)
	    		if(!isNaN(firm_proto))
	    			ids.push(object.points[firm_proto].firm_id);
    	}else{
    		object = this.state.clickedObject;
    		ids = this.state.click.ids;
    	}
    	var smtModel = await this._retrieveModelEdges(layerCfg, Number(layerCfg.currDate.toString().split(".")[0]), ids);
    	if(smtModel.edges !== undefined){
	    	var tmpCF = this._parseSmtModel(smtModel, this.state.localFirms, this.state.localFirmIndicesObj, ids);
//			var scale = d3.scaleQuantile()
//				.domain(tmpCF.domain)
//				.range(ARC_COLORS.map((c, i) => i));

	    	var greenScale = chroma.scale(['#47ff4d','#006803']).domain([tmpCF.domain[0] ,tmpCF.domain[1]]);
	    	var redScale = chroma.scale(['#ffb5b5','#f73600']).domain([tmpCF.domain[0] ,tmpCF.domain[1]]);

	    	//var matching = /\d+/g;
			tmpCF.flowVecs.forEach(a => {
				//a.fillColor = ARC_COLORS[scale(a.amount)];
//				var fill = [];
//				var rgbString = d3.interpolateSpectral((a.amount - tmpCF.domain[0])/(tmpCF.domain[1] - tmpCF.domain[0]));
//		    	var match = matching.exec(rgbString);
//				while(match !== null){
//					fill.push(parseInt(match[0]));
//					match = matching.exec(rgbString);
//				}
				//a.fillColor = fill;
				a.targetColor = greenScale(a.amount).rgb();
                a.sourceColor = redScale(a.amount).rgb();

            });
			this.state.smtCashFlows = tmpCF;
			//getFillColor: Math.round(linearMapping(smtCashFlows.domain, LOCAL_ARC_WIDTH_RANGE, d.amount))//[0,0,0,0],
	    	var flows = this.state.smtCashFlows.flows;
	    	var intervals = this.state.smtCashFlows.domain;
	    	var totalFlow = flows[0] + flows[1] + flows[2];
	    	totalFlow = this._getSuffixString(totalFlow);
	    	var min = this._getSuffixString(intervals[0]);
	    	var max = this._getSuffixString(intervals[1]);
	    	label = (	<div>
	        				Overall Flow: {totalFlow}<br />
	        				Outgoing: {(flows[2]/(flows[0] + flows[1] + flows[2]) * 100).toFixed(2)}%<br />
	        				Ingoing: {(flows[0]/(flows[0] + flows[1] + flows[2]) * 100).toFixed(2)}%<br />
	        				Max edge: {max[0]}{max[1]}<br />
	        				Min edge: {min[0]}{min[1]}
	    				</div>
	    	);
	    	var x = this.state.click.x;
	    	var y = this.state.click.y;
	        this.setState({ click: { x, y, clickedObject: object, label: label, ids:ids } });
    	}else{
    		this.state.smtCashFlows = {};
            this.setState({ click: { x, y, clickedObject: object, label: "", ids:[] } });
    	}
    };

    _updateLayerCfg = (layerCfg, refresh = false) => {
        this.setState({ layerCfg: layerCfg }, () => {
            if(refresh){
            	this._clearClickAndHover();
            	this._onViewOptionChange(null);
            }
            else
    	        if(this.state.click){
    	        	console.log("Updating SMT after layercfg update call");
    	        	this._updateSMT();    	        	
    	        }

        });
    };

    _clearClickAndHover(){
    	this.setState({
    			...this.state,
    			hover: {
    	            x: 0,
    	            y: 0,
    	            hoveredObject: null
    	        },
    			click: {
    	        	x: 0,
    	        	y: 0,
    	        	clickedObject: null
    	        }
    		});
    }

    _onSelectedSectorsChange = (layerCfg) => {
        let localSelectedFirmsBySector = this.state.localFirms.filter(
            firm => {
                return layerCfg.selectedSectors.has(firm.edv_e1);
            }
        );
        this.setState({layerCfg: layerCfg, localSelectedFirmsBySector: localSelectedFirmsBySector});
    };
    _onMapStyleChange = (event) => {
        this.setState({mapStyle: event.target.value});
    }

    _onViewOptionChange = async (event) => {
        const currView = event == null ? this.state.layerCfg.view : event.target.value;
        if (event !== null && currView === this.state.layerCfg.view) {
            return;
        }
        const currLayerCfg = {
            ...this.state.layerCfg,
            view: currView
        };
        this.setState({initFinished: false}, async () => {
        	this._clearClickAndHover();
	        if (currView === 'localView') {
	            let localState = await this._loadLocalViewData(currLayerCfg);
	            this.setState({...localState, initFinished: true});

	        } else if (currView === 'regionalView') {
	            let regionState = await this._loadRegionalViewData(currLayerCfg);
	            this.setState({...regionState, initFinished: true});
	        }
	    });
    }

    _onRegionLevelChange = async (event) => {
        const currRegionLevel = event.target.value;
        if (currRegionLevel === this.state.layerCfg.regionLevel) {
            return;
        }
        const currLayerCfg = {
            ...this.state.layerCfg,
            regionLevel: currRegionLevel
        };
        this.setState({initFinished: false}, async () => {
            let regionState = await this._loadRegionalViewData(currLayerCfg);
            this.setState({...regionState, initFinished: true});
        });
    }

    _calcHexagonRadius = (zoom, minZoom, maxZoom) => {
        const minHexRadius = 25, maxhexRadius = 6000;
        let scaleChange = (maxZoom - zoom) / (maxZoom - minZoom);
        let hexRadius = Math.round(scaleChange **3 * (maxhexRadius - minHexRadius)  + minHexRadius);
        return (hexRadius - hexRadius % minHexRadius);
    }

    _onViewStateChange = ({viewState}) => {
        const {layerCfg} = this.state;
        if (viewState.zoom < viewState.minZoom || viewState.zoom > viewState.maxZoom) {
            return;
        }
        let zoom = Math.round(viewState.zoom);
        if (this.state.viewState.zoom !== viewState.zoom) {
            let radius = this._calcHexagonRadius(zoom, viewState.minZoom, viewState.maxZoom);
            if (radius !== layerCfg.hexagonRadius) {
                const currLayerCfg = {
                    ...layerCfg,
                    hexagonRadius: radius
                };
                this.setState({viewState: viewState, layerCfg: currLayerCfg});
                if(this.state.click.clickedObject !== undefined){
                	var pickedObject = this.deckGL.pickObject({x: this.state.click.x, y:this.state.click.y, radius: this.state.layerCfg.hexagonRadius, layerIds: ['local-hexagon']});
                	if(pickedObject !== null)
                		this._updateSMT(pickedObject.object);
                	}
            } else {
                this.setState({viewState: viewState});
            }
        }
        else {
            this.setState({viewState: viewState});
        }
    }

    _getSuffixString = (value) => {
    	let suffix = [value, ""];
        if (suffix >= 10**12) {
            suffix[0] = (value /(10**12)).toFixed(3);
            suffix[1] = " Tn";
        } else if (value >= 10**9) {
        	suffix[0] = (value / (10**9)).toFixed(3);
            suffix[1] = " Bn";
        } else if (value >= 10**6) {
        	suffix[0] = (value / (10 ** 6)).toFixed(3);
            suffix[1] = " M";
        } else if (value >= 10**3) {
        	suffix[0] = (value / (10 ** 3)).toFixed(3);
            suffix[1] = " K";
        }

        return suffix;
    }

    render() {
        const { classes } = this.props;
        const {viewState, hover, click, initFinished} = this.state;
        // const currentT = Math.floor((Date.now() / 10) % 100)/100;

        let loader = (<div></div>);
        if (!initFinished) {
            loader = (<div className={classes.loader}></div>);
        }
        return (
            <div onContextMenu={event => event.preventDefault()}>
                {hover.hoveredObject && (
                    <div
                        className={classes.hoverLabel}
                        style={{transform: `translate(${hover.x}px, ${hover.y}px)`}}
                    >
                        {hover.label}
                    </div>
                )}
                {loader}
                <CtLayerCfg
                	//models={this.state.models}
                    layerCfg={this.state.layerCfg}
                    mapStyle={this.state.mapStyle}
                    oenaceLevelOne={this.state.oenaceLevelOne}
                    onSelectedSectorsChange={layerCfg => this._onSelectedSectorsChange(layerCfg)}
                    onLayerCfgChange={(layerCfg, refresh) => this._updateLayerCfg(layerCfg, refresh)}
                    onMapStyleChange={event => this._onMapStyleChange(event)}
                    onViewOptionChange={event => this._onViewOptionChange(event)}
                    onRegionLevelChange={event => this._onRegionLevelChange(event)}
                />
                <DeckGL
                	ref={deck => { this.deckGL = deck; }}
                    onWebGLInitialized={this._onWebGLInitialize}
                    layers={[
//                    	new Layer({
//                    		pickable: true,
//                    		onClick: (info, event) => {
//                    			this._clearClickAndHover();
//                    		}
//                    	}),
                    	new CtLayers({
                        regionalBins: this.state.regionalBins,
                        localSelectedFirmsBySector: this.state.localSelectedFirmsBySector,
                        localFirms: this.state.localFirms,
                        firmsYearlys: this.state.firmsYearlys,
                        euRatiosYearlys: this.state.euRatiosYearlys,
                        euRatioYearlySumMax: this.state.euRatioYearlySumMax,
                        smtCashFlows: this.state.smtCashFlows,
                        pickable: true,
                        onHover: hover => this._onHover(hover),
                        onClick: click => this._onClick(click),
                        layerCfg: this.state.layerCfg,
                        viewState:this.state.viewState,
                        currentTime: this.state.currentTime / 100,
                    })]}
                    viewState={viewState}
                    onViewStateChange={({viewState}) => this._onViewStateChange({viewState})}
                    controller={{type:MapController}}
                >
                    <ReactMapGL
                        mapboxApiAccessToken={TOKEN}
                        mapStyle={this.state.mapStyle}
                    >
                        <div style={{position: 'absolute', left: '13px', top: '13px'}}>
                            <NavigationControl showZoom={false} />
                        </div>
                    </ReactMapGL>
                </DeckGL>
                <TimelineSlider
                    layerCfg={this.state.layerCfg}
                    onLayerCfgChange={layerCfg => this._updateLayerCfg(layerCfg)}
                />
                <DetailsPanel
                	layerCfg={this.state.layerCfg}
                	state={this.state.click.label}
                />                
        </div>
        );


    }

    _hexToRgbA = (hex) => {
        let c;
        if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
            c= hex.substring(1).split('');
            if(c.length== 3){
                c= [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c= '0x'+c.join('');
            return [(c>>16)&255, (c>>8)&255, c&255];
            //return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',1)';
        }
        throw new Error('Bad Hex');
    }

    _sortObjectProps = (obj, callback, context) => {
      var tuples = [];

      for (var key in obj) tuples.push([key, obj[key]]);

      tuples.sort(function(a, b) {
        return a[1] < b[1] ? -1 : a[1] > b[1] ? 1 : 0
      });

      var length = tuples.length;
      while (length--) callback.call(context, tuples[length][0], tuples[length][1]);
    }

}

export default withStyles(MAP_VIS_STYLE)(MapVis);
