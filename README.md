## Make sure to rerun `npm install` for the client and `python pg_import.py` after every `git pull` (see below)
# data
Currently locations of all the headquaters and about 1/4 of the branches are in the database. 
So far only headquaters are taken into account for the visualization. 

`line_num` refers to the lines in the file **Standorte--DISTINCT.csv** (the header line is also taken into consideration)

# <a id="setup"></a> setup
Open the file `db.env.template` in directory *ctvis*, and change the postgres configuration (username, password, host, port, database name) accordingly. Save the file as `db.env`. The database, if not present, will be created anew.  

## import data into postgres
(do **NOT** import the csv files directly using postgres commands, preprocessing is performed in the python script)

Python 3.6 or newer is needed. The script also relies on `python-dotenv` and `psycopg2`, to install, run
```
pip install python-dotenv
pip install psycopg2
```
(from *elevated command line* or add ` --user` at the end of each command)


To import data into postgres, go to directory *ctvis/py*, run

```
python pg_import.py
```

which will create 14 tables and import data from directory *ctvis/py/geodata/\*.csv* into the corresponding tables:
```
bezirk
bundesland
eu_ratios
gemeinde
location
main_sec_distr_bkz
main_sec_distr_bl_kz
main_sec_distr_gkz
main_sec_distr_okz
oenace2008de
oenace2008en
ort
plz
sector
```
## server
Install Node.js

go back to directory *ctvis*, run
```
npm install
```

## client
go to directory *ctvis/client*, run
```
npm install
```

feel free to change the **favicon.ico** located at *ctvis\client\public*

# run

To run the project, there are ~~two~~ three options. Some patience is required either way.

## option 0 (only need to be built once unless client code is modified.)

go to directory *ctvis/client*, run
```
npm run build
```
(you may want to remove all the contents in the directory *ctvis/client/build* before rebuild due to change of the source code)

After the client code is successfully built, go to directory *ctvis*, run
```
npm run fast
```

## option 1
go to directory *ctvis*, run
```
npm run server
```
and start a new cmd window, go to directory *ctvis*, run
```
npm run client
```
## option 2
go to directory *ctvis*, run
```
npm run dev
```

# a few more words
For dababase, I downloaded the installer from [here](https://www.postgresql.org/download/) (Windows x86-64, version 11.2). The installation should be quite straightforward. The database can also be installed in the Windows Linux Subsystem, so not to pollute the Windows installation. It is possible to use a GUI to visualize and interact with it using the software [PGAdmin](https://www.pgadmin.org/).

To get a basic understanding of reactjs, I followed this [tutorial](https://reactjs.org/tutorial/tutorial.html). 

Tutorial for deckgl can be found [here](http://vis.academy/#/building-a-geospatial-app/setup) for basics and [here](http://vis.academy/#/custom-layers/setup) for custom layer. The [official documentation](http://deck.gl/#/documentation/overview/introduction) is for the latest version. Some APIs I used might be deprecated by now (e.g. now `getWidth` should be used in place of `getStrokeWidth` for ArcLayer, ), in case you want to upgrade to the latest version (in *ctvis/client/package.json*) for new features. 


# settings
The *settings panel* can be toogled on/off by clicking the *settings* button on the top left below the compass. 

In the "local" view, each sector can be (de)selected. 

![settings button]( ./res/settings.png)
